### Horizontal

```
<span class="spacer-h-xs"></span>
<span class="spacer-h-s"></span>
<span class="spacer-h-m"></span>
<span class="spacer-h-l"></span>
<span class="spacer-h-xl"></span>
<span class="spacer-h-xxl"></span>
<span class="spacer-h-xxxl"></span>
```

### Vertical

```
<span class="spacer-v-xs"></span>
<span class="spacer-v-s"></span>
<span class="spacer-v-m"></span>
<span class="spacer-v-l"></span>
<span class="spacer-v-xl"></span>
<span class="spacer-v-xxl"></span>
<span class="spacer-v-xxxl"></span>
```