Text Fields are implemented using Angular Material, visit [Angular Material Input](https://material.angular.io/components/input/overview) for all functional documentation.

## Imports

```
import {MatInputModule} from '@angular/material/input';
```

## Appearance - Outline

Text Fields **must** have the 'outline' appearance style set to the form field wrapper. 

```
<mat-form-field appearance="outline">
	...
</mat-form-field>
```

## Appearance - Fill

Text Fields **must** have the 'fill' appearance style set to the form field wrapper. 

```
<mat-form-field appearance="fill">
	...
</mat-form-field>
```