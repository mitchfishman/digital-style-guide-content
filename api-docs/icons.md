## Configure the application to use the TMO SVG icons

In order to get started, make sure the application **copies the TMO SVG icons into its assets folder**. This can be done by adding the following configuration to the app's `angular.json` configuration.

```
{
  "projects": {
    "client": {
      "architect": {
        "build": {
          "options": {
            "assets": [
              {
                "glob": "**/*",
                "input": "node_modules/@tmo/component/icon/assets",
                "output": "/assets"
              }
            ]
          },
        }
      }
    }
  }
}
```

On the **app's AppModule** make sure you register the `TmoIconRegistry`, s.t. SVG icons are registered with Angular material's registry.

```
import { TmoIconRegistryModule } from '@tmo/components/icon';

@NgModule({
  imports: [
    ...
    TmoIconRegistryModule.forRoot()
  ],
  ..
})
export class AppModule {}
```

### Enabling SVG Sprite optimizations

There's the possibility to opt-in to SVG sprite optimization. If activated, at build-time of an application, it's templates are being parsed in search for `<tmo-icons>` that are being used. They are collected and inlined into the `index.html` document as an SVG sprite.

> Note, not all icons might be found, especially when the icon name is dynamically constructed, e.g. `<tmo-icon [iconName]="getIconName()">`.

To enable the SVG sprite follow these steps:

1. install the `@angular-builders/custom-webpack` npm package.
2. Change the app's `angular.json` build configuration, adjusting the builder name and adding the `indexTransform` property.

```
{
  ...
  "projects": {
    "client": {
      ...
      "architect": {
        "build": {
          "builder": "@angular-builders/custom-webpack:browser",
          "options": {
            "indexTransform": "node_modules/@tmo/component/iconbuilder/index-svg-embed-transform.ts",
            ...
          },
        },
        "serve": {
          "builder": "@angular-builders/custom-webpack:dev-server",
          ...
        },
      }
    }
  }
}
```

By enabling this, you should see an inlined `<svg>` tag containing the identified SVG icons, just after the `<body>` tag.

### Imports
On the app's AppModule make sure you register the TmoIconRegistry, s.t. SVG icons are registered with Angular material's registry
```

import { TmoIconRegistryModule } from '@tmo/components/icon';

@NgModule({
  imports: [
    ...
    TmoIconRegistryModule.forRoot()
  ],
  ..
})
export class AppModule {}

```


## Properties

| Attribute                         | Description                            |
| --------------------------------- | -------------------------------------- |
| `@Input() iconName`| Name of the material icon < tmo-icon iconName="chevron_right" > < /tmo-icon >|
| `@Input() inline`| Boolean parameter to vertically align the icon in the container |



## Size classes 

Default size of the icon is 24px, and below are global classes for different sizes - 



|Class                  | Size |
| --------------- |-----|
|          `tmo-icon-xxs`  |12px         |
|          `tmo-icon-xs`| 16px           |
|         ` tmo-icon-sm`| 20px         |
|          `tmo-icon-lg`| 32px           |


## Color classes 

Below are global classes for different colors for the icon - 

| Class      | Color    | Token name | Hex value |
| ---------| ------- | ---------- | -------- |
|`tmo-icon-red` | Red |--color-base-error | #e8200d |
| `tmo-icon-accent`| Magenta | --color-magenta-default | #e20074 |
| `tmo-icon-green`| Green | --color-base-success | #078a14 |




