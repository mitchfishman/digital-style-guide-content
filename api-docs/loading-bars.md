Loading bars is implemented using Angular Material, visit [Angular Material Progress bar](https://material.angular.io/components/progress-bar/overview) for all functional documentation.

## Imports

```
import {MatProgressBarModule} from '@angular/material/progress-bar';
```
