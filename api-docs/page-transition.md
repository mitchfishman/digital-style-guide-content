Transition animation is implemented using Angular animations feature, visit [animations](https://angular.io/guide/animations) for all functional documentation.

Similarly, Route transition animations can be done using Angular route-animations feature, visit [route-animations](https://angular.io/guide/route-animations) for all functional documentation.
