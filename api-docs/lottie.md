Lottie animation is implemented using ngx-lottie library, visit [ngx-lottie](https://www.npmjs.com/package/ngx-lottie) for all functional documentation.

## Imports

```
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';

```

## Properties

| Attribute                         | Description                            |
| --------------------------------- | -------------------------------------- |
| `@Input() options: Object{}`| Animation init configuration |
| `@output() animCreated: anim;`| Dispatched after the lottie successfully creates animation |
| `styles?: Partial<CSSStyleDeclaration>`| Custom styles object. Bound to 'ngStyle' |
| `width?: string;`| Animation container width in pixels |
| `height?: string;`| Animation container height in pixels |
| `role?: string;`| Element role for web accessibility |
| `alt?: string;`| Alt text for web accessibility |
| `aria-label?: string;`| Aria-label text for web accessibility |
