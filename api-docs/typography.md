## Using pixels or rems in typography
Our site uses rems, and not pixel values, to define sizing whenever possible. For example, `line-height` or `max-width`.

When a developer is choosing how to style a font, use the variables we have defined. If there isn’t a variable for your font size, use rems.

## Font sizes

When styling fonts in SCSS, try to use existing variables. You can also use helper classes in the HTML.

| Font size     | Class         | Sass Variable         |
| ------------- | ------------- | ------------- |
| Normal (16px) |  | `$base-font-size`  |
| Small (14px)  | `.text-small` |`$small-font-size` 
| Tiny (12px)   | `.text-tiny`  | `$tiny-font-size`  |
| Legal (12px)  | `.text-legal` | `$legal-font-size` |


## The rem() function
If you need to style something that is 42px, for example, use the [rem() utility function](https://gitlab.com/tmobile/digital/ux/style-guide/-/blob/tmo/master/libs/shared/ui/tmo-styles/src/lib/config/_sass-utilities.scss) and it will calculate the rems for you.

For example: `letter-spacing: rem(-0.78px); max-width: rem(42px);`


## Headings

Heading styles can be applied by using a heading HTML tag e.g. `<h1>My heading</h1>`, or by using a class e.g. `<h3 class="heading-1">My heading</h3>`. If you use the class, you should still use some type of heading tag.

| Heading type         | Class                   | Element | Pixel value (Desktop)| Pixel value (Mobile)|
| -------------------- | ----------------------- | ------- | ---|---|
| Heading 1            | `.heading-1`            | h1      | 72px   |56px|
| Heading 2            | `.heading-2`            | h2      | 56px|48px|
| Heading 3            | `.heading-3`            | h3      | 48px|40px|
| Heading 4            | `.heading-4`            | h4      | 40px|32px|
| Heading 5            | `.heading-5`            | h5      | 32px|24px|
| Heading 6            | `.heading-6`            | h6      | 24px|20px|
| Section header       | `.section-header`       |         | ||
| Section header small | `.section-header-small` |         |||
| Eyebrow              | `.eyebrow`              |         |||

## Links

Use `.link` to apply the link style to an anchor element. 

Anchor elements within a paragraph will automatically have this style applied.


## Alignment

| Alignment | Class          |
| --------- | -------------- |
| Left      | `.text-left`   |
| Right     | `.text-right`  |
| Center    | `.text-center` |

## Font weight

| Font weight | Class                 |
| ----------- | --------------------- |
| 400         | `.font-weight-normal` |
| 700         | `.font-weight-bold`   |
| 800         | `.font-weight-800`    |

## No-wrap

Use `.text-no-wrap` to create a block where the text contents will not break. This is handy for wrapping the T-Mobile name:

```
<span class="text-no-wrap">T-Mobile</span>
```


## Font Families

| Sass Variable   | Value                                                                              |
| --------------- | ---------------------------------------------------------------------------------- |
| `$heading-font` | TeleNeoWeb, Arial, Helvetica, 'Helvetica Neue', sans-serif;                 |
| `$body-font`    | BlinkMacSystemFont, -apple-system, Segoe UI, Roboto, Helvetica, Arial, sans-serif; |


## Monospaced numbers

Use `.monospaced-number` to style numbers by sizing them to same width. This shows tabular-width styling for numbers without using a monospaced font. We typically use this style to improve scannability when numbers in a column need to align or be a constant width. TeleNeo and TT Norms will offer different numberforms for some characters. 

```
<span class="monospaced-number"> 1234.45 </span>
```
