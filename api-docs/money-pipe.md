Money pipe is implemented using Angular format currency function, visit [Angular Format Currency](https://angular.io/api/common/formatCurrency) for all functional documentation.

## Imports

```
import { TmoMoneyPipe } from '@tmo/components/money-pipe';
```


## Parameters

The parameters to the money pipe must be provided in the following sequence - 

| Parameter                         | Description                            | Values              |
| --------------------------------- | -------------------------------------- | ------------------- |
| `showComma`| When set to true, applies comma to the amount when the amount is less than 9999. | `true`, `false` |
| `showCents` | When set to true, allows to shows cents even when the amount is a whole number.  |  `true`, `false` |
| `showMinus` |When set to true, adds a minus sign prefix to the amount. |  `true`, `false`|

