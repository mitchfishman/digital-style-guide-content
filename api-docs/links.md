Links are implemented using HTML anchor element.

## Types

Anchor element wrapped inside a div tag must use class `link`.

```
<div>
  <a class="link" href="#">Body link</a>
</div>
```

Anchor elements representing external links must use class `external-link`.

```
<div>
  <a class="link external-link" href="#">External link</a>
</div>
```