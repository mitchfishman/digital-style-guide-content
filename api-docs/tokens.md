Design tokens should be used to in component scss files to avoid hard coding values.

## Imports

```css
@import '~@tmo/components/tmo-styles/token-variables';
```

## Usage

```css
.button.primary {
  background: var(--color-brand);
}
```
