Dividers are implemented using Angular Material, visit [Angular Material Divider](https://material.angular.io/components/divider/overview) for all functional documentation.

## Imports

```
import {MatDividerModule} from '@angular/material/divider';
```


## Sizes

Default divider width will be 1px


```
<mat-divider class="divider-heavy">
</mat-divider>
```




| Modification | Class              |
| ------------ | ------------------ |
| 2px divider width        | `divider-medium` |
| 3px divider width        | `divider-heavy` |


      