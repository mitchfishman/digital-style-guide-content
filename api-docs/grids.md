The grid system is implemented using a modified version of [Blueprint CSS](https://blueprintcss.dev/). In our version, all selectors are specified inside a ‘tmo-layout’ attribute instead of ‘bp’, and we have modified the breakpoints and spacing to match our standard ones. You can take a look at the [Blueprint CSS docs](https://blueprintcss.dev/docs) for more examples.

## Breakpoints

The breakpoints listed under [Responsive Columns](https://blueprintcss.dev/docs/grid) have been replaced with these:

| Blueprint's breakpoint | Our replacement |
| ---------------------- | --------------- |
| Small devices          | 320px           |
| Medium devices         | 768px           |
| Large devices          | 1024px          |
| Extra Large devices    | 1280px          |

## Spacing Variables

The spacing variables listed under [Spacing Utilities](https://blueprintcss.dev/docs/util-spacing) have been replaced with these:

| Blueprint's breakpoint | Our replacement variable | Our replacement value |
| ---------------------- | ------------------------ | --------------------- |
| `$xs-spacing`          | `$spacing-xs;`           | `16px`                |
| `$sm-spacing`          | `$spacing-sm;`           | `24px`                |
| `$md-spacing`          | `$spacing-med;`          | `40px`                |
| `$lg-spacing`          | `$spacing-lg;`           | `56px`                |

Primarily these were changed to not cause confusion with our spacing variables, as the names are very similar.