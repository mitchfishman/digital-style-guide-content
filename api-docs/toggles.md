Toggles are implemented using Angular Material, visit [Angular Material slide-toggle](https://material.angular.io/components/slide-toggle/overview) for all functional documentation.

## Imports

```
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
```


## Wrapper class

All ON/OFF toggles use the class `tmo-toggle-control`.

```
  <mat-slide-toggle
    class="tmo-toggle-control"
    [labelPosition]="labelPosition"
    [disabled]="disabled"
    [checked]="checked"
  >
    <span class="toggleLabel" id="matToggleLabel">5G</span>
  </mat-slide-toggle>

```


## Toggle in list

To use ON/OFF toggles in a list, add the class `tmo-list-toggle` to the list `<ul>` element.

Add ` list-divider` class to `<ul>` element, to implement lists with dividers. 


```
  <ul class="tmo-list-toggle list-unstyled list-divider">
    <li>
      <mat-slide-toggle
        class="tmo-toggle-control"
        [labelPosition]="labelPosition"
        [checked]="isChecked"
        [disabled]="isDisabled"
      >
        Label
      </mat-slide-toggle>
    </li>
 </ul>

```