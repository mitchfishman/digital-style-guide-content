Carousels are implemented using Angular wrapper library [ngx-swiper-wrapper](https://www.npmjs.com/package/ngx-swiper-wrapper/v/8.0.2) Visit [Swiper](http://idangero.us/swiper/) for all functional documentation.

## Imports

```
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
};

@NgModule({
  ...
  imports: [
    ...
    SwiperModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
```

## Properties

| Attribute                         | Description                            |
| --------------------------------- | -------------------------------------- |
| `@Input() carouselId: string` | Unique carousel Id to scope carousels individually          |
| `@Input() pageConfig: any`      | Config to modify default values to determine number of cards as per viewport     |

## PageConfig 

| Parameter                         | Description                            | Default Value             |
| --------------------------------- | -------------------------------------- | ------------------- |
| `desktopSlidesPerPage`    | Determines number of slides to be displayed in desktop view at a time. | `4` |
| `tabletSlidesPerPage` | Determines number of slides to be displayed in tablet view at a time.          |    `3`   |
| `mobileSlidesPerPage`      | Determines number of slides to be displayed in mobile view at a time.    |  `1`   |
| `desktopSlidesPerGroup`    | Determines number of slides to define for group sliding in desktop view.  . | `4` |
| `tabletSlidesPerGroup` | Determines number of slides to define for group sliding in tablet view.     |    `3`   |
| `mobileSlidesPerGroup`      | Determines number of slides to define for group sliding in mobile view.    |  `1`   |
| `mobileSpaceBetweenCards`    | Determines space between slides in px. | `8` |
| `cardsAutoHeight` | Set to true and slider wrapper will adapt its height to the height of the currently active slide.  |    `false`   |
| `autoplayDelay`      | Determines delay between transitions (in ms).    |  `5000`   |
| `carouselWithTimer`      | Set to true will enable the timer version of carousel   |  `false`   |


## Swiper Parameters

| Parameter                         | Type                            | Default Value             | Description          |
| --------------------------------- | -------------------------------------- | ------------------- |  ------------------- |
| `direction` | string          |    `horizontal`   |  Set direction of the swiper, could be 'horizontal' or 'vertical' (for vertical slider).      |
| `spaceBetween`      | number    |  `0`   |      Distance between slides in px.  |
| `slidesPerView`    | number or 'auto' | `1` |  Number of slides per view (slides visible at the same time on slider's container).    |
| `slidesPerGroup` | number          |    `1`   |  Set numbers of slides to define and enable group sliding.    |
| `autoHeight`      | boolean    |  `false`   |     Set to true and slider wrapper will adapt its height to the height of the currently active slide.  |
| `scrollbar`      | boolean    |  `false`   |     Object with scrollbar parameters.  |
| `autoplay`    | boolean | `false` |  Object with autoplay parameters.   |
| `a11y` | object/boolean	          |      |  Object with a11y parameters or boolean true to enable with default settings Accessibilty params.    |
| `pagination`    | object |     |  Object with pagination parameters ( set clickable: false, If false then clicking on pagination button will prevent slide transition. Only for bullets pagination type.  |
| `navigation` | object          |       |  Object with navigation parameters.      |
| `breakpoints`    | object |     |  Allows to set different parameter for different responsive breakpoints (screen sizes). Not all parameters can be changed in breakpoints, only those which are not required different layout and logic, like slidesPerView, slidesPerGroup, spaceBetween, slidesPerColumn. Such parameters like loop and effect won't work.  |


## Paddle Classes

The default behavior of the navigation paddles would be to hide on page load and show on carousel container hover.

```
<tmo-carousel class="show-paddles">
  
</tmo-carousel>
```


| Class | Modification             |
| ------------ | ------------------ |
| `show-paddles`   | Display navigation paddles all the time on the carousel |
| `hide-paddles`  | Hide navigation paddles all the time on the carousel  |
