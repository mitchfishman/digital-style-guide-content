List is implemented using Angular Material, visit [Angular Material List](https://material.angular.io/components/list/overview) for all functional documentation.

## Imports

```
import {MatListModule} from '@angular/material/list';
```
