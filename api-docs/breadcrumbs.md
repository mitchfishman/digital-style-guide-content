## Imports

```
import { TmoBreadcrumbsModule } from '@tmo/components/breadcrumbs';

```


## Properties

| Attribute                         | Description                            |
| --------------------------------- | -------------------------------------- |
| `@Input() pagebreadcrumbObj: { path: string; description: string, externalLink: boolean }[]`| Path of child components (or) external url for navigation.|
| `@Input() breadcrumbObj: { path: string; description: string, externalLink: boolean }[]`| Path of routing url (or) external url for navigation. This property is required only for root level breadcrumb implementation. |
| `@Input() breadcrumbId: string;`| Unique id for each breadcrumb to avoid data conflict. |


