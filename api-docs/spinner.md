Spinner is implemented using Angular Material, visit [Angular Material Progress spinner](https://material.angular.io/components/progress-spinner/overview) for all functional documentation.

## Imports

```
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
```