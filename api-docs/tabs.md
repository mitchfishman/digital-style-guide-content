Tabs are implemented using Angular Material, visit [Angular Material Tabs](https://material.angular.io/components/tabs/overview) for all functional documentation.

```
import { MatTabsModule } from '@angular/material/tabs';
```
