Item is a custom shared component used for basic content layout.

```
import { TmoItem } from '@tmo/components/item';
```

## Properties

Attributes are used to set modifies that affect the layout.

| Attribute                              | Description                                                 |
| -------------------------------------- | ----------------------------------------------------------- |
| `@Input() attributes: ItemAttributes;` | Attributes are used to set modifies that affect the layout. |

## Interface

Example set isStacked to true and the image will stack over the content.

```
export interface ItemAttributes {
  isUnstackable?: boolean;
  isStacked?: boolean;
  isGrid?: boolean;
  isDivided?: boolean;
  isCompact?: boolean;
  isCenter?: boolean;
}
```

## Modifiers

The power of the item component comes form its use of modifiers. The attributes above add the class modifiers to the component.

| Modifier                             | Description                                                                                                         |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------------- |
| `isUnstackable` adds `isunstackable` | This overwrites the default behavior for images to stack (image above content) on mobile                            |
| `isStacked` adds `isstacked`         | Sets images to always be stacked (image above content).                                                             |
| `isGrid` adds `isgrid`               | Used in conjunction with `<div tmo-layout="grid" class="grid-border">` to set the `isgrid` class to style as a grid |
| `isDivided` adds `isdivided`         | Adds divider lines to items                                                                                         |
| `isCompact` adds `iscompact`         | Create a compact default layout                                                                                     |
| `isCenter` adds `iscenter`           | Center text in the component                                                                                        |

## Nested Modifiers

| Modifier  | Element                                                  | Description                                            |
| --------- | -------------------------------------------------------- | ------------------------------------------------------ |
| `isright` | `<tmo-item-image [ngClass]="{ isright: item.imgRight}">` | You have the option of aligning an image to the right. |

## Add ons

| Name           | Description                                      | Examples                                                                                        |
| -------------- | ------------------------------------------------ | ----------------------------------------------------------------------------------------------- |
| Line Clamp     | Clamp content to set number of lines (default 3) | `<p tmoDocsLineClamp [innerHtml]="item.description"></p>` or `<h3 [tmoDocsLineClamp]="2"></h3>` |
| Fallback Image | Add a fallback image if image fails to load      | `<img [src]="item.image" tmoDocsImgFallback [alt]="item.imageAlt" />`                           |
