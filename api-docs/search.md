The search input is a custom implementation of Angular Material's autocomplete component. It exists in the style guide not as a reusable component, but as a how-to guide.

Visit [Angular Autocomplete](https://material.angular.io/components/autocomplete/overview) for all functional documentation.


## Imports

```
import {MatAutocompleteModule} from '@angular/material/autocomplete';
```


## Customizations
1. A search icon and clear icon have been added and styled to be closer to the edges than a normal input. The search icon also changes to the brand's accent color when a user is typing.

2. The dropdown results are populated using observables. As a user types into the input ("iph"), we subscribe to those changes using[ Angular's reactive forms with the valueChanges observable](https://angular.io/guide/observables-in-angular#reactive-forms). Then, a service fetches suggested items (e.g. "iPhone", "iPhone 11", "iPhone X"). These items get assigned to another observable, called `searchSuggestions$`. The material dropdown then subscribes to the `searchSuggestions$` observable in the template and displays the items. This implementation was first used on the [T-Mobile search page](http://t-mobile.com/search) and was developed from [Angular's guide on typeahead suggestions](https://angular.io/guide/practical-observable-usage#type-ahead-suggestions). 

## Accessibility
Search/Autocomplete dropdowns **must** include the directive ```tmoDocsOptionReader``` to insure menu options are read by screen readers. 
