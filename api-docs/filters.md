

```
import { FilterDesktopComponent } from './filter-desktop/filter-desktop.component';
import { FilterMobileComponent } from './filter-mobile/filter-mobile.component';

```

## Properties

| Attribute                         | Description                            | Values              |
| --------------------------------- | -------------------------------------- | ------------------- |
| `@Input() multi: boolean`    | Provides multiple choice for filters true or false.        |                     |
| `@Input() togglePosition: string`  | Toggles multiple filter positions.                      |                     |
| `@Input() initOpenAll: boolean`          | Initiates open all.               |                     |
| `@Input() iconName: string`        | Displays an image icon preview.                        |                     |.
| `@Input() headerHeight: number`        | Changes the header height by number.                         |                     |.

  ## Types

The filter button only apears in mobile view. 
Mobile view uses the base class `cta-button`.

```
Note: button only appears on mobile
<button
  mat-stroked-button
  class="glu-filter-button cta-button"
  color="primary"
  (click)="openDialog()"
>
  Filter
</button>
```
