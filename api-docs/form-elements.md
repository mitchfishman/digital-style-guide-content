### Optional input with tooltip

Module added to mat-components.module.ts:
```
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
```    

### Required input with hint

Module added to mat-components.module.ts:
```
import { MatInputModule } from '@angular/material';
```

### Input with prefix and suffix

Module added to mat-components.module.ts:
```
import { MatInputModule } from '@angular/material';
```

### Text area with max rows
```
import { MatInputModule } from '@angular/material';
 ```
 
### Selector

Module added to mat-components.module.ts:
```
import { MatInputModule } from '@angular/material/select';
```