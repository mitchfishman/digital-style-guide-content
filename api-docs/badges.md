Badges are implemented using Angular Material, visit [Angular Material Badge](https://material.angular.io/components/badge/overview) for all functional documentation.

## Imports

```
import { MatBadgeModule } from '@angular/material/badge';
```