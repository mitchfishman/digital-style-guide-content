Modals are implemented using Angular Material, visit [Angular Material Dialog](https://material.angular.io/components/dialog/overview) for all functional documentation.

## Imports

```
import {MatDialogModule} from '@angular/material/dialog';
```

## Styling

Modal styles must be applied when you open the dialog using the `modal` class.

```
const dialogRef = this.dialog.open(ExampleModalComponent, {
    panelClass: 'modal',
});
```

See the code tab for a more complete example.

## Sizes

Default modal  max-width will be 960px (L)


```
    const dialogRef = this.dialog.open(ExampleModalComponent, {
      autoFocus: false,
      panelClass:[ 'modal', 'modal-medium'],
      maxHeight: '90vh',
      data: { title: this.title },
    });
```




| Class |    Modification           |
| ------------ | ------------------ |
|  `modal-medium`     | 720px modal max-width   |
| `modal-small`      |  540px modal max-width   |



See the code tab for a more complete example.