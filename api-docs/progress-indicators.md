### Determinate Spinner

Module added to mat-components.module.ts:

```
import { MatProgressSpinnerModule } from '@angular/material';
```

### Indeterminate Spinner

Module added to mat-components.module.ts:

```
import { MatProgressSpinnerModule } from '@angular/material';
```

### Determinate Bar

Module added to mat-components.module.ts:

```
import { MatProgressBarModule } from '@angular/material';
 ```  

### Indeterminate 

Module added to mat-components.module.ts:

```
import { MatProgressBarModule } from '@angular/material';
```
