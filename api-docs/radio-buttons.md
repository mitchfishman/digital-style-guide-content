Please refer to the official [Angular Material docs for radio buttons](https://material.angular.io/components/radio/overview). You can find the documentation for the radio button groups below.


**NOTE:** To view the API documentation for the `tmo-expansion-panel-radio-button` component, visit the [Checkboxes API](/components/checkboxes/api) page.

<!-- api-docs(radio-group) -->