Accordion is implemented using Angular Material, visit [Angular Material Expansion Panel](https://material.angular.io/components/expansion/overview) for all functional documentation.

```
import { MatExpansionModule } from '@angular/material';
```

## Properties

| Attribute                         | Description                            |
| --------------------------------- | -------------------------------------- |
| `@Input() togglePosition: MatAccordionTogglePosition`    | The position of the expansion indicator. |
| `@Input() multi: boolean` | Whether the accordion should allow multiple expanded accordion items simultaneously.          |
| `@Input() hideToggle: boolean`      | Whether the expansion indicator should be hidden.     |
| `@Input() displayMode: MatAccordionDisplayMode`      | Display mode used for all expansion panels in the accordion. Currently two display modes exist: default - a gutter-like spacing is placed around any expanded panel, placing the expanded panel at a different elevation from the rest of the accordion. flat - no spacing is placed around expanded panels, showing all panels at the same elevation.     |


## Methods

| Method                         | Description                            |
| --------------------------------- | -------------------------------------- |
| closeAll   | Closes all enabled accordion items in an accordion where multi is enabled. |
| openAll | Opens all enabled accordion items in an accordion where multi is enabled.          |