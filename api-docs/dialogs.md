Dialogs are implemented using Angular Material, visit [Angular Material Dialog](https://material.angular.io/components/dialog/overview) for all functional documentation.

## Imports

```
import {MatDialogModule} from '@angular/material/dialog';
```

## Styling

Dialog styles must be applied when you open the dialog using the `modal-dialog` class.

```
const dialogRef = this.dialog.open(ExampleModalComponent, {
    panelClass: 'modal-dialog',
});
```

See the code tab for a more complete example.