Dropdowns are implemented using Angular Material, visit [Angular Material Select](https://material.angular.io/components/select/overview) for all functional documentation.

## Imports

```
import { MatSelectModule } from '@angular/material/select';
```

## Appearance - Outline/Fill

Dropdowns **must** have the 'outline/fill' appearance style set to the form field wrapper. 

```
<mat-form-field appearance="outline">
	...
</mat-form-field>
```
```
<mat-form-field appearance="fill">
	...
</mat-form-field>
```

## Accessibility
Dropdowns **must** include the directive ```tmoDocsOptionReader``` to insure menu options are read by screen readers. 
