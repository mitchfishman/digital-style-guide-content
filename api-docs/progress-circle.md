## Imports

```
import { TmoProgressCircleModule } from './lib/progress-circle';
```


## Parameters

The parameters to the money pipe must be provided in the following sequence - 

| Parameter                         | Description                            | Values              |
| --------------------------------- | -------------------------------------- | ------------------- |
| `value`| This is the value which displays the progress of the indicator out of the provided maximum value. | Can be any number less than or equal to the maximum value|
| `maxValue` | This is the maximum value which indicates 100% of the progress indicator.  | Can be any number  |
| `indicatorType` |This describes the type of progress indicator. If the type is not days or is not required to be displayed within the circle, then this input can be ignored. | `days`, `GB`|
| `size` |This is the size of the progress circle. |  `lg`, `sm`|

