## Sass Variables
Many elements (Angular Material or otherwise) have color variations, reference their documentation to find out how to use them. If you are creating something new or custom, you can reference these Sass variables to use the official brand colors. Favor these Sass variables (rather than hardcoding the hex values) to stay up to date with theme color updates.

### Brand Colors

| Sass Variable            | Hex Code  |
| ------------------------ | --------- |
| `$tmo-magenta-brand`     | `#e20074` |
| `$tmo-magenta-light`     | `#f66db3` |
| `$tmo-magenta-dark`      | `#ba0060` |
| `$tmo-magenta-very-dark` | `#a10053` |

### Grays

| Sass Variable | Hex Code  |
| ------------- | --------- |
| `$tmo-white`  | `#ffffff` |
| `$tmo-gray05` | `#f8f8f8` |
| `$tmo-gray10` | `#f2f2f2` |
| `$tmo-gray20` | `#e8e8e8` |
| `$tmo-gray30` | `#cccccc` |
| `$tmo-gray40` | `#9b9b9b` |
| `$tmo-gray60` | `#6a6a6a` |
| `$tmo-gray70` | `#4c4c4c` |
| `$tmo-gray85` | `#262626` |
| `$tmo-gray80` | `#333333` |
| `$tmo-black`  | `#000000` |

### Accent Colors

| Sass Variable      | Hex Code  |
| ------------------ | --------- |
| `$tmo-green`       | `#078a14` |
| `$tmo-green-light` | `#51d85e` |
| `$tmo-red`         | `#e8200d` |
| `$tmo-yellow`      | `#e3b721` |

### Additional Color Aliases

| Sass Variable               | Alias for     | Intended use                                                                |
| --------------------------- | ------------- | --------------------------------------------------------------------------- |
| `$default-text-color`       | `$tmo-gray85` | Default for non-interactive text on a light background                        |
| `$disabled-text-color`      | `$tmo-gray60` | Indicates that the element would normally be interactive, but it's disabled |
| `$default-light-text-color` | `$tmo-white`  | Default for non-interactive text on a dark background                         |

## Background Color Classes

All of the major theme colors have a corresponding background color class. These are handy for applying to simple elements such as page backgrounds.


`.bg-tmo-magenta-brand`, `.bg-tmo-magenta-dark`, `.bg-tmo-gray10`

```
<div class="bg-tmo-magenta-brand">...</div>
```