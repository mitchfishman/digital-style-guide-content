## Margin and padding classes
Use the formula `{property}-{side}-{size}` 
for example, `<h1 class="margin-bottom-40"></h1>`
or  `{property}-{size}` to apply to all sides
for example, `<h1 class="margin-40"></h1>`

| Property     | Side         | size     |
| ------------- | ------------- | ------------- |
| margin | top | 2  | 
| padding | right | 4  | 
|  | bottom | 8  | 
|  | left | 16  | 
|  | horizontal | 24  | 
|  | vertical | 32  |
|  |  | 40  | 
|  |  | 56  | 
|  |  | 80  | 
|  |  | 120  | 

## Reset classes
| Margin | Padding | Line Height |
| ---------- | ---------- | ---------- |
| `.margin-0` | `.padding-0` | `.text-line-height-0` |
| `.margin-top-0` |`.padding-top-0` | |
| `.margin-right-0` | `.padding-right-0` | |
| `.margin-bottom-0` | `.padding-bottom-0` | |
| `.margin-left-0` | `.padding-left-0` | |
| `.margin-horizontal-0` | `.padding-horizontal-0` | |
| `.margin-vertical-0` | `.padding-vertical-0` | |

## CSS variables
Use these variables when you need to set spacing in your styles. This should be secondary to using the margin and padding classes provided above.

| Token Name     | Pixels         | REM units      | 
| ------------- | ------------- | ------------- | 
| --spacing-1	| 1px | 0.0625rem |	
| --spacing-2	| 2px | 0.125rem	|
| --spacing-4	| 4px | 0.25rem	|
| --spacing-8	| 8px | 0.5rem	|
| --spacing-12	| 12px | 0.75rem	|
| --spacing-16	| 16px | 1rem	|
| --spacing-24	| 24px | 1.5rem	|
| --spacing-32	| 32px | 2rem	|
| --spacing-40	| 40px | 2.5rem	|
| --spacing-56	| 56px | 3.5rem	|
| --spacing-80	| 80px | 5rem |
| --spacing-120	| 120px | 7.5rem |
