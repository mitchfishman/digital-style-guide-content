Date pickers are implemented using Angular Material, visit [Angular Material Datepicker](https://material.angular.io/components/datepicker/overview) for all functional documentation.

## Imports

```
import { MatDatepickerModule } from '@angular/material/datepicker';
```

## Appearance - Outline

Date pickers  **must** have the 'outline' appearance style set to the form field wrapper. 

```
<mat-form-field appearance="outline">
	...
</mat-form-field>
```