Pagination is implemented using ngx-pagination, visit [ngx-pagination](http://michaelbromley.github.io/ngx-pagination/#/ ) for all functional documentation.

## Properties

| Attribute | Description | 
| --------------------------------- | -------------------------------------- | 
| `id: string` | If you need to support more than one instance of pagination at a time, set the id and ensure it matches the id set in the PaginatePipe config. | 
| `maxSize: number` | Defines the maximum number of page links to display. Default is 7. | 
| `itemsPerPage: number` | Set default number of items to display on load | 
| `currentPage: number` | Set default active page on load | 
| `showControls: boolean` | Show/Hide the select list to change itemsPerPage count | 
 
 <!-- api-docs(pagination) -->

