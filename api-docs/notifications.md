## Properties

| Attribute                                          | Description                                                                                                                                |
| -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| `@Input() color: 'accent' \| 'error' \| 'warning'` | The color of the banner                                                                                                                    |
| `@Input() fixed: boolean`                          | Can the banner be dismissed? If false, the banner will have a dismiss (x) button on the right side.                                        |
| `@Input() class: string`                           | An additional class to apply to the banner element                                                                                         |
| `@Input() icon: string`                            | Name of an icon that should be displayed on the left side of the banner. If the banner has an icon, the text content will be left-aligned. |
| `@Input() duration: number`                        | A duration in seconds that a banner should be displayed for. Once the duration is over, the banner will animate away.                      |
| `@Input() contentTemplate: TemplateRef<any>`       | A reference to a template whose content should be rendered as banner content. You can also nest content into the component.                |
| `@Output() closed: EventEmitter`                   | An event that emits when the banner is closed.                                                                                             |

<!-- api-docs(banner) -->
