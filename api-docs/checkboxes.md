Checkboxes are implemented using Angular Material, visit [Angular Material Checkbox](https://material.angular.io/components/checkbox/overview) for all functional documentation.

## Imports

```
import { MatCheckboxModule } from '@angular/material/checkbox';
```

## Vertical

Checkboxes can be aligned vertical if you add `vertical` to the form field wrapper.

```
<div class="form-field vertical">
  ...
</div>	
```