*Lottie* is a development library for Android, iOS, Web, and Windows [from the Airbnb design team](https://airbnb.io/lottie) that parses Adobe After Effects animations exported as JSON with Bodymovin and renders them natively on the mobile and on the web.

### Set up for designers
To export an After Effects animation, we use the Bodymovin' plugin. Adobe Creative Cloud users can [install Bodymovin from Adobe Exchange](https://exchange.adobe.com/creativecloud.details.12557.bodymovin.html). Learn more about working with After Effects and exporting using Lottie for the web [here](https://airbnb.io/lottie/#/after-effects?id=more-bodymovin-info). Note that the Design Systems team does not provide the animation file. Please reach out to the journey UX designer for the file.

### Best practices
Lottie can be used:
- To show complex, vector-based animated scenes as backgrounds.
- To indicate waiting or loading in a custom way, tailored to the experience rather than generically. For example, an animated airplane outline when waiting to look at international roaming rates.
- For morphing one shape into another, such as an indeterminate spinner seamlessly turning into a checkmark.

### Accessibility
- For animations that should appear for context, like an image, use the parameters ```role="img"```, provide alternate text and an aria-label with ```alt="clock animation" aria-label="clock animation"```
- Use CSS to allow users to disable the motion.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/image/)

