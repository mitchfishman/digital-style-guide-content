Item is a custom shared component used for various content layouts. It can stack, align, and separate content using pre-defined configurations.

### Best practices

#### Do

- Consider using the `tmoDocsLineClamp` for long titles and text.
- Consider using [Fallback image directive](https://styleguide.docs.t-mobile.com/#/components/fallback-image/usage) `tmoDocsImgFallback` for images.

#### Don’t

- No don'ts.

### Accessibility

- Carefully consider the logical focus orders and appropriate labels for each element.

