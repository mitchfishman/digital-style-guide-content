## Brand fonts
Brand fonts are used in heading 1 to 6. We've switched to the new T-Mobile's brand font called **TeleNeo**. For Metro by T-Mobile, we use **TT Norms**.

## System fonts
For everything else, we use system fonts. What does that mean? Well, your typeface should depend on platform.
<ul>
	<li>On iOS, we see <strong>SF Pro</strong>. Designers use this font in XD for demonstration purposes only, as it has the widest "worst-case scenario" for copy width.</li>
	<li>On Android, we see <strong>Roboto</strong>.</li>
	<li>On MacOS, we see <strong>Helvetica Neue</strong>.</li>
	<li>On Windows, we see <strong>Segoe UI</strong>.</li>
</ul>


## Accessibility in typography
We use our _Header_ and _Heading class_ font designations apart from traditional HTML `<h1>` or `<h4>` tags, for example. Developers should only use `h` tags for **page hierarchy**, and use heading **classes** to style them.
Designers can assist by calling out a logical heading hierarchy, separate from typography.
