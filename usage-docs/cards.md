
A Card is a UI design pattern that groups related information in a flexible-size container.

- Cards are used for grouping information. <br>
A single card will typically include a few differnet types of media, such as an image, a title, a synopsis, sharing icons, or a call-to-action button/link.
- Cards present a summary and link to additional details. <br>
A card is usually short and offers a linked entry point to further details, rather than the full details themselves.
- Cards resemble physical cards. <br>
Cards use a border around the grouped content, and the background color within the card that differs from background color of the underlying canvas. These visual-design elements create a strong sense that the different bits of information contained within the border are grouped together.
- Cards allow for flexbible layouts. <br>
Cards allow for varying height to accomondate differing amount or types of content, but typically the width is fixed from one card to the next.

### Best practices

#### Do

- Use cards to effectively lay out information & actions of discrete types
- Each card should have one subject
- Cards should be of similar importance to user tasks
- Put UI controls inside cards

#### Don’t

- Put multiple subjects into a single card

### Accessibility
- Carefully consider the logical focus order. If the entire card is taking the user to another page, the entire card should be focusable, then followed by the interactive elements within the card.
