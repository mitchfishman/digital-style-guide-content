Skeleton loader is an animated placeholder to let the users know that information is still loading. It gives a general idea of what and how much content will be loaded.

### Best practices

#### Do
- Use when data takes a while to load on the page.
- Use to minimize jumping and shifting content on the page once real content is loaded.

#### Don’t
- Use when data will be loaded immediately on the page.

### Accessibility
- Use motion that is slow and steady. 
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/animation/)



