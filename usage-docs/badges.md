Badges are a component that overlays an icon to indicate that some new activity has occurred. We use Badges in the shop to indicate that the user has placed something in their cart.

### Best practices

#### Do

- Overlay badges over an icon. You may also include a number.

#### Don’t

- Use badges in any other context.

### Accessibility
- More info at [Angular Material Component](https://material.angular.io/components/badge/overview#accessibility)
