## Responsive breakpoints

Instead of using media queries, use these mixins to style content at specific breakpoints. 

* for-phone-only 

* for-tablet-only 

* for-tablet-down 

* for-tablet-up 
 
* for-desktop-sm-down 
 
* for-desktop-sm-up

* for-desktop-down

* for-desktop-up
 
* for-desktop-xl-only 
 
* for-ie-browser-only

###### Example
```
@include for-phone-only {
  .my-class {
    margin-left: $spacing-xs;
  }
}
```

## Page containers
Unless a page is laid out in AEM, most pages will need to be wrapped in a page container, such as `<div tmo-layout="container">`. 

This container should wrap most content inbetween the Unav header and footer. It adds margin to the left  and right and these change at each breakpoint. On mobile, the left and right margins are 1rem/16px. On desktop, they are 3.5rem/56px. Desktop breakpoints also have a fixed-width so content doesn't stretch too far on extra-wide viewports. View the css in the [*layout.scss* file](https://gitlab.com/tmobile/digital/ux/style-guide/blob/d85363592dbbb38c5575d3edb91d79319ac25ba9/libs/shared/ui/tmo-styles/src/lib/base/_layout.scss).

###### Usage
Add the attribue `tmo-layout="container"` to any element or apply the `.page-container` class, although the attribute is preferable.

###### Example
```
<div tmo-layout="container">
  
  <!-- MyTMO Homepage - Balance and Quicklinks -->
  <div tmo-layout="grid" class="margin-bottom-sm">
    <section tmo-layout="12@sm 6@md 6@lg">
      <tmo-docs-balance></tmo-docs-balance>
    </section>

    <section tmo-layout="12@sm 6@md 6@lg">
      <tmo-docs-quicklinks></tmo-docs-quicklinks>
    </section>
  </div>

<div>
```

###### When not to use it
If you need something to stretch edge-to-edge on desktop *and* mobile, place that element outside of the page container. An example of this would be the [notifications banner](/components/notifications/examples), which stretches edge-to-edge on both desktop and mobile.

## Full-bleed (mobile only)
Some designs call for content to stretch edge-to-edge on mobile, while the rest of the content around it does not. In this case, you would use the `.full-bleed-mobile` class. 

###### Usage
Apply the `.full-bleed-mobile` class it to any element that lives inside of a page container and it will stretch edge-to-edge on mobile only. 

###### Example
```
<div tmo-layout="container">
	<h1 class="heading-3"></>
	<p>Some paragraph text<p>
	<mat-nav-list class="full-bleed-mobile">
	...
	</mat-nav-list>
</div>
```

## Margin bottom or margin top?
When adding space between elements, try to use margin-bottom instead of margin-top. This creates code consistency and a rule-of-thumb that’s easy to follow.

You may notice that header elements (h1, h2, etc.) are an exception, as they have built-in spacing on the top and bottom.

###### Usage
To apply padding and margin in a .html file, use helper classes like `.margin-bottom-xs`, referenced in the [Spacing guide examples](/foundation/spacing/examples).

To apply padding and margin in a .scss file, use mixinis like `$spacing-xs`, referenced in the [Spacing guide API](/foundation/spacing/api)
or view the [*spacings.scss*](https://gitlab.com/tmobile/digital/ux/style-guide/blob/5fb9f0429415c3ffef4ccd1537288c7ae50bf4d2/libs/shared/ui/tmo-styles/src/lib/config/spacing.scss) file.

## .hidden
The `hidden` class hides elements visually but still makes them available to screen readers. This replaces the `.sr-only` class. Check out the [progress bar](/components/progress-bars/examples) for an example.

## .full-width
The `full-width` class sets an element to width:100%.
















