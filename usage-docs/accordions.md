Accordions are an organizational tool for text-based information. Accordions are characterized by showing and hiding page content through expansion and collapse.

### Best practices
- Try to keep labels to a single line. In the case that the label must be longer than one line, keep text vertically centered.
- Limit Accordion content to text, hyperlinks, or tertiary buttons/links. Do not include other controls or images.
- The icon for expand/collapse action should be upward or downward chevron. Note that the plus and minus icons are deprecated.
- Accordions should be 6 responsive columns wide on desktop and 12 on mobile. This means on desktop, there can be two accordions side by side.

#### Do

- Use when the audience needs only a few key pieces of content on a single page. Therefore, audience can spend their time more efficiently focused on the few topics that matter. 
- Use when the information is restricted to very small spaces, such as on mobile devices. Minimize excessive scrolling. 
- Put accordion control on the right side of the accordion. 
- Keep your headline short and clear. It will help with navigating through the information.

#### Don't

- Use accordions on content on the page that audience needs to answer their questions. In this case, relevance trumps page length.

### Accessibility

- The expansion-panel aims to mimic the experience of the native `<details>` and `<summary>` elements.
- Because the header acts as a button, additional interactive elements should not be put inside of the header.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/expander/)
