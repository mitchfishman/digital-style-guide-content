When an image fails to load on a webpage, a 404 error is thrown and a broken image icon is displayed by the browser. To create a better user experience, a developer can add the tmoDocsImgFallback Angular directive to the <img> element. If the intended image fails to load, the fallback image will be displayed.

<!-- example(default-fallback-image) -->

If a different image is desired, an image url can be passed to the directive. If the custom image fails as well, the default fallback image will be displayed. The directive does not resize the fallback image so make sure it is the same size as the intended image.

<!-- example(custom-fallback-image) -->