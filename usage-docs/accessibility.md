**Accessibility** was mandated by the U.S. government as part of The Americans with Disabilities Act or ADA. The ADA is a civil rights law that prohibits discrimination against individuals with disabilities in all areas of public life, including jobs, schools, transportation, and all public and private places that are open to the general public.

In 1998, Congress amended the Rehabilitation Act of 1973 with Section 508, that requires Federal agencies to make their electronic and information technology (EIT) accessible to people with disabilities. The World Wide Web Consortium (W3C), established in 1994, regulates industry members for the compatibility of standards for the World Wide Web. To address the growing divide between rising Internet usage and its limited accessibility among individuals with disabilities, W3C created the “Web Accessibility Initiative: Web Content Accessibility Guidelines” (WAI-WCAG) to address this issue. The guidelines were originally published in 1999, and consisted of 14 points for accessible design for individuals with different types of disabilities.

WCAG 2.1 encompasses Section 508 and extends the criteria internationally.

### A11y

A11y, pronounced "A eleven Y", is numeronym that represents the first and last letters of the word Accessibility and the 11 letters that fall between them.

#### Why A11y?

Not only can T-Mobile suffer litigation for not adhering to A11y standards, creating the best user experience for all clientele is paramount to keeping T-Mobile the industry leader.
	
## Our commitment to accessibility
T-Mobile has committed to **WCAG AA** compliance. 

This living style guide and pattern library is developed with semantically correct accessible components with appropriate ARIA markup for users of assistive technologies.

You will still need to test the accessibility of your applications before release and ensure that it meets the WCAG Standard at the AA Level.

In order to build accessible components for T-Mobile products,  you will need to follow the accessibility guidance for our interactive components.

**Contact the Accessibility Resource Center for help anytime.**
* [#help-accessibility-digital](https://slack.com/app_redirect?channel=CQ6PW2KN1)
* arc-support@t-mobile.com

### How to assess for accessibility — Web
Accessibility is both User Experience and code. At T-Mobile, our process is to have Design Systems teams assess UXs in two phases.
* First, through a Design Systems UX review
	* Schedule this with Designers in [#help-design-systems](https://slack.com/app_redirect?channel=CNLCRK48K) Slack
	* Attend Design Systems office hours
	* Designers and Product are encouraged to attend, but not yet ready for Developers
* Next, when developers have been assigned, a Design Systems developer and Style Guide review will be scheduled.
	* This can be scheduled in a few ways:
		* During/after Design Systems UX review
		* Direct contact with Mitch Fishman or Vivian Huang
		* Create a second intake in [#help-design-systems](https://slack.com/app_redirect?channel=CNLCRK48K) Slack

A [Sign-up sheet can be found here](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=ZXP4uS) for reviews and office hours.

Since our interactive components have guidance, our developers can walk feature teams through these best practices. **Learn more in the [Feature teams](/resources/feature-teams/usage) documentation.**

## What makes something accessible?

All people should be able to use T-Mobile’s products. T-Mobile values inclusion, not just because it’s a requirement, but because we believe that creating products that everyone can use (especially the most vulnerable people) makes a better product for everybody.

<h5 class="section-header section-header-small">There are 4 broad categories of disabilities</h5>

**Vision**
* People with no or very limited sight still use the internet with assitive technology called screen readers. 
* People with poor vision need sufficient contrast or to be able to zoom in on interfaces.

**Motor**
* People with limited mobility may not be able to use a mouse or touchpad accurately.

**Hearing**
* People with no or very limited hearing need to be able to consume video or audio content through captions or transcripts.

**Cognitive**
* People with learning disabilities like dyslexia or ADHD or even people who don’t speak english as their first language should be able to understand and consume our content.

A product is accessible if all people, including those with disabilities, can use its features. This means that the product is [perceivable](https://www.w3.org/TR/WCAG21/#perceivable), [operable](https://www.w3.org/TR/WCAG21/#operable), [understandable](https://www.w3.org/TR/WCAG21/#understandable) and [robust](https://www.w3.org/TR/WCAG21/#robust). [link to each on the WCAG spec]

### Key questions:
* Is there anything in our product that a screen reader user, deaf, low vision or color blind user would not be able to perceive?
* Can all functions of your product be performed with only a keyboard?
* Is it possible to complete tasks without fine motor control?
* Could a 9th grader understand the written content in your product?
* Are all of the interactions and workflows are easy to understand?
* Do the labels make sense to a screen reader?
* Can the product be operated by a wide range of user agents and devices such as keyboard, mobile touch, screen readers, or switch devices

## Educational Resources
The T-Mobile Accessibility Resource Center has accessibility coaches to help teams master the craft of building accessible experiences.

**Contact the Accessibility Resource Center for help anytime.**
* [#help-accessibility-digital](https://slack.com/app_redirect?channel=CQ6PW2KN1)
* arc-support@t-mobile.com

<h5 class="section-header section-header-small">Full blown online courses</h5>
Free:

* [Accessibility by Google, 7hr](https://www.udacity.com/course/web-accessibility--ud891) <span class="text-small"><em>Udacity.com</em></span>
* [A11y Cast](https://www.youtube.com/playlist?list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g) <span class="text-small"><em>YouTube</em></span>
	
Subscription-based learning:
* [Accessibility for the  Web](https://www.linkedin.com/learning/accessibility-for-web-design/welcome) *LinkedIn Learning, 2hr*
* [Web Accessibility Series + Code Quizzes](https://teamtreehouse.com/library/web-accessibility-compliance) *Team Treehouse, 2hr*
* [Web Accessibility: Getting Started](https://www.pluralsight.com/courses/web-accessibility-getting-started) *Pluralsight, 1.5hr*

Reference Websites
* [WebAim.org (Web Accessibility In Mind)](https://webaim.org/)
* [WebAim Contrast Check tool](https://webaim.org/resources/contrastchecker/?fcolor=FFFFFF&bcolor=E20074)
* [*A List Apart*: Accessibility Category Archive](https://alistapart.com/blog/topic/accessibility/)

Books 
* [*Accessibility for Everyone* by Lara Kalbag](https://abookapart.com/products/accessibility-for-everyone) <small>*A Book Apart*</small>
* [*Design for Real Life* by Eric Meyer and Sara Wachter-Boettcher](https://abookapart.com/products/design-for-real-life) <small>*A Book Apart*</small>

#### Join the conversation

T-Mobile Slack 
* [#discuss-accessibility](https://slack.com/app_redirect?channel=CDUV37QAZ) &mdash; General discussion about accessibility
* [#help-accessibility-digital](https://slack.com/app_redirect?channel=CQ6PW2KN1) &mdash; Technical assistance for all teams (Managers, content, design, devs)

Twitter <img src="https://cdn.tmobile.com/content/dam/t-mobile/theme/assets/social/tweet_black.svg" alt="" aria-hidden="true">
* Hashtag [#A11y](https://twitter.com/hashtag/a11y?src=hash)
