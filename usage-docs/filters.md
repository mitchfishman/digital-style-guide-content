Filters are a selection control to reduce the number of results based on users’ inputs. Filters are usually paired with the chips component to indicate the active selections.

### Best practices

#### Do
- Limit title to less than 25 characters.
- Use sentence case and no punctuation.

#### Don’t
- Use chips as anything other than filters without prior approval.

### Accessibility
- More info on expansion panel at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/expander/)
- More info on chips at [Angular Material Component](https://material.angular.io/components/chips/overview)

