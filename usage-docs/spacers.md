<div style = "width: 660px">

## **Best practices**
#### Design

T-Mobile uses standardized spacing elements. Spacing is measured in pixels.

<img src="https://tmobile.egnyte.com/dd/WioWmwisbY/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>
	
<img src="https://tmobile.egnyte.com/dd/Qkd6cJrILi/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>

</div>	

#### Common spacing patterns:

<img src="https://tmobile.egnyte.com/dd/THfo4CSs14/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>

* Between header and title: M (32/40px)
* Between title and body copy: XS (16/16px)
* Between stacked primary and secondary buttons: XS (16/16px)
* Body copy and next element: M (32/40px)
* General: S (24px)
* Between bottom of content and footer: XL (56/80px)