Notification framework is a collection of usage requirements and recommendations on use for our notifications. This framework guide is for general usage and comparison between components. 

Specific usage, components, and styling should be found within each component on the [Style Guide](https://styleguide.docs.t-mobile.com/#/).



### Full screen interruption 

User interruption level is **high**. 

Full screen interruption should be used for end of the road type of errors like 404, 505, "account suspended", temporary unavailable data, and so on. Header and footer navigation should still be implemented for nearly all full screen interruptions. 

<!-- example(full-page-notification) -->


### Dialog
User interruption level is **high**. 

Dialogs are used to help the user make an action without leaving the page. The action/request within the dialog should always be related to what the user was immediately trying to accomplish. They should be used when the user the required to make a choice or needs to know something specific before continuing. 

<!-- example(dialog-notification-framework) -->


### Text field error label
User interruption level is **medium**. 

Use text error messages in form elements, when user provided the wrong information into the text field. Always write error messaging when designing interfaces that use text field. 

<!-- example(text-input-notification-framework) -->


### Inline notification
User interruption level is **medium**. 

Use inline notification for larger, section level errors. For example, use inline notification when the page itself is working, but a section of a page is experiencing an error. Please include slide animation as part of your experience design so users do not see content randomly pop-up on a page. Slide-animation inline notification example can be found [here](http://styleguide.docs.t-mobile.com/#/components/page-transition/examples).
Be careful with how these are written and observe the rules written in the UI Kit and the Style Guide.

<!-- example(inline-notification) -->
<!-- example(list-action-notification) -->


### Banners
User interruption level is **medium**. 

Banners should be used for general system wide notifications. They are less visible unless they appear sometime after load, so take caution while using banners for important notifications or steps that are crucial for the user to complete. 
Follow these three rules for understanding hierarchy with banners and implementing them in designs:
1. Sticky banners are more visible than non-sticky banners.
2. Non-dismissible banners rank higher on the notification/disruption scale than dismissible banners. 
3. Banners with CTAs rank higher on the notification/disruption scale than banners without CTAs. 

<!-- example(banner-notification) -->


### Tidbit 
User interruption level is **low**. 

Tidbit should be used when providing information to the user by being injected into the body of the page. Tidbits can also load along with the page content and appear as a part of page design and not an update/interruption. The information they provide could be important or not important for the user to progress/succeed. 

<!-- #### Do

1. Use Tidbit for information regarding changes to account or billing
2. Use Tidbit for low priority warning messages
3. Use Tidbit for statistics or other information relevant to user's decision

#### Don't
1. Use Tidbit for high priority errors -->

<!-- example(tidbit-icon-notification) -->


### Snackbar
User interruption level is **low**. 

Snackbars should be used when something is changed and a success message is necessary, or something was attempted and failed and a notification/error of a higher importance in not needed. 

<!-- example(snackbar-notification) -->


