### T-Mobile logos
The preferred logo is Magenta. The logo is only used in the header of the universal navigation (UNav). Do not use the logo elsewhere without prior approval. 

#### T logo
<img src="https://cdn.tmobile.com/content/dam/t-mobile/ntm/branding/logos/corporate/tmo-logo-v3.svg" width=75px/>	


#### Full logo
We typically don't use the full logo on our digital properties.
<!-- <img style="display: block" src="https://tmo.widen.net/content/lynltblgpr/original/T-Mobile_New_Logo_Primary_RGB_M-on-W_Transparent.png?collectionShareName=85sieirj&download=true&u=unknown&x.app=portals&x.portal_shortcode=pro74u7a" height=50px/> -->


### More resources
https://tmap.t-mobile.com/portals/ywaqvfby/T-MobileBrandPortal for logos, fonts, and overall brand direction.
