Lists define the layout of the page content or groups of elements, stacking them vertically. A list can be used to display content related to a single subject. The content can consist of multiple components such as text, images, etc.

Lists present content in a way that makes it easy to identify a specific item in a collection and act on it. 

### Best practices

#### Do

- Use a list when you need to display information in a ranking, hierarchy, or series of steps.
- Sort the lists in logical ways that make content easy to scan.
- Assign default class of <code>.divide-middle</code>. This adds a border in-between each list item. Top and bottom borders can also be added to the list using <code>.divide-top</code> and <code>.divide-bottom</code>. 

#### Don't
- Divide list items using Material's <code>mat-divider</code>. This will cause screen readers to read out each divider.


### Types of List

#### Basic List

Basic lists can take different forms, depending on the needs of the list. A basic list should include a Title and Divider.
Icons, Subtitles, Helper Text, and Images are optional.

#### Action List

Action list uses `<button>` elements. It should not navigate users to another page, but should instead perform an action or do something on the same page.

#### Navigation List

Navigation lists navigate the user to another page and start a new experience flow. Navigation lists should include a Title, Divider, and Chevron.
Icons, Subtitles, Helper Text, Images, and additional labels are optional.

#### Ordered List

The order of list items is strict. List items are marked with numbers. 

### Accessibility

- The focusable area should cover the entire list blade if the blade contains an interactive element.




