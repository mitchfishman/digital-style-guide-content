The search component allows the user to input a query in the text field and view suggested inputs options in the added dropdown. Users then can select one of the options as their input from the autocomplete dropdown. It's built using Angular Material's [Form field](https://material.angular.io/components/form-field/overview) and [Autocomplete component](https://material.angular.io/components/autocomplete/overview) and is customized slightly.

### Types of search component

#### Default
The default autocomplete dropdown lists out the suggested query that the user can choose from.

#### Autocomplete with groups
The suggested queries can be grouped in some meaningful way.

#### Autocomplete with text highlighting
In addition to showing the list of results, the component can also visually highlight the query that had already been entered by the user.

### Accessibility
- Ensure meaningful focus order for users. Users can submit their query by clicking the search icon, pressing the Enter key, or clicking any of the suggested results. They can navigate from the search icon to the input and the clear button using the Tab key.
- Users can erase their query by clicking the clear button to type Backspace or Delete.
- Users can navigate through the dropdown using the Up Arrow and Down Arrow keys.
- More info on text fields at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/text-input/)
- More info on autocomplete listbox at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/listbox-autocomplete/)
