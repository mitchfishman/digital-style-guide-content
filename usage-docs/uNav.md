The universal navigation (UNAV) appears at the top of the page and provides navigation links to the different parts of the site. On mobile, the links collapse into groups.

UNAV are authorable and should not be hard-coded to any page. Please consult the UNAV governance team for any changes to the UNAV. 

Slack channel: [#help-unav](https://slack.com/app_redirect?channel=C01T7AXDBM1)

### Accessibility
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/nav/)



