Carousels are UI elements containing several pages of browsable content. Some carousels have timers that automatically advance content. They contain large imagery and text to draw the user's attention and to provide an overview of key T-Mobile offerings and features.

### Accessibility
- Logical focus order and updates for the screen reader may be complex interactions for developers. Please read the examples and API documentation on the Style Guide carefully and reach out to SuperGlue team for code reviews.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/carousel/)

