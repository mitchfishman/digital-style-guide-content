Tidbits are non-disruptive message blocks that appear in the body of a page. A Tidbit can contain an image, but the focus should be on the text. Tidbits can load along with the page content and appear as a part of page design and not an update/interruption.

Tidbit component is part of the [notification framework](https://styleguide.docs.t-mobile.com/#/frameworks/notification-framework/usage). Carefully evaluate options for notifications.

## Best practices
	
### Do
- Use a tidbit to provide more information regarding changes to accounts or billing.
- Use a tidbit for statistics or other information relevant to a user's decision.
	
### Don't
- Use tidbits for system-caused or any high-priority errors.
- Span to full-screen width. Tidbits are not banners. 

### Accessibility
- Tidbits themselves are not focusable. Define logical tab order for interactive elements within a tidbit.
