### Best practices

#### Do
- Carefully consider the screen reader experience in terms of what is the main label, secondary label, and any description needed to associate with the option label.
- If your radio label will span more than one line, make sure to **add** the attribute `[radioCentered]="false"` to `tmo-card-radio-button`, otherwise the radio will fall below the first line of label text.

#### Don't

- Try to code from scratch without referencing the examples on Style Guide first.

### Accessibility
- When the design includes interactive elements in between radio buttons, it increases the complexity for development. Please follow the examples carefully and reach out to the SuperGlue team for code reviews.
- The keyboard behavior is different from regular radio groups. Users can use arrow keys to navigate between options in regular radio groups and use tab key to exit the radio group. In this component, the tab order goes from the radio button to the next interactive element in between, then to the next radio button.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/radio/)



<!-- ### Note on centering

If your radio label will span more than 1 line, make sure to **add** the attribute `[radioCentered]="false"` to `tmo-card-radio-button`, otherwise the radio will fall below the first line of label text.

![](https://i.imgur.com/pAgDX3Z.png)

![](https://i.imgur.com/I8O8MGo.png) -->
