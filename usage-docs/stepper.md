Input stepper (“Stepper”) is a set of interactive elements used to increase or decrease the number input of a form control.

### Best practices

#### Do

- Define minimum and maximum number for the input.
- Provide user feedback and disable action buttons when reaching out of range inputs.

#### Don’t

- Use the component for large number input.

### Accessibility

- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/stepper-input/)

