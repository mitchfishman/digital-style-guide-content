A progress spinner ("spinner") is a circular indicator of progress and activity. Spinners may be used for single component or full page load states.

### Best practices

#### Do

- Use determinate spinners when the operation will take a known amount of time to complete.
- Use indeterminate spinners when the operation will take an unknown amount of time to complete.
- Use when an operation takes longer than 1/10th of a second to complete. 

#### Don't

- Combine spinners with other types of loaders on the same page.

### Accessibility
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/progress-bar/)
