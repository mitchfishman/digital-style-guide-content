Snackbars are non-disruptive message blocks that appear at the bottom of a page. 

Snackbar is part of the [notification framework](https://styleguide.docs.t-mobile.com/#/frameworks/notification-framework/usage). Carefully consider all options for notifications.

### Best practices

#### Do
- Use for simple verification type of notification for the user (eg.: “Message sent”, or, “Changes saved”).

#### Don't
- Include any types of action other than "undo".
- Use snackbars to notify the user of system-caused errors or warnings. 
- Use snackbars as part of the critical user flows.

### Accessibility
- Snackbars are deep in DOM, presenting keyboard navigation challenges.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/snackbar-toast/)


<!-- ## Timing
Snackbars should exist for 6 seconds. They should not be dismissible. 
It is important, therefore, that the action associated with the message is not critical for users to engage with. For example, “Changes saved.” with “Undo” CTA is not a necessary action for the user, as they should be able to switch their choice back if they wish to.

## CTA
Snackbar call to action should be one thing: **Undo** 
These two options have the same action. That is, they reverse the action taken by the user.

## Spacing
Internal spacing is built into the component.

## Drop shadow
Drop shadow is built into the component for dark and light theme.

## States
Mobile width should not deviate from what is shown.
Web length should be tightly controlled. Max width: 4 columns. 
Web snackers can be shorter. Minimum width is width of mobile snackbar.
Web version should not be two lines. -->
