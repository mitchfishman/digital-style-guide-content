Divider is a line that visually separates the content into groups and/or group content in lists and layouts. Dividers increase users' understanding of the content by clearly identifying how the content is grouped and organized.

### Best practices
Divider should be placed in between topics of content or between lists of items.

### Accessibility
- The divider is a not-focusable element and should have attribute `aria-hidden="true"`.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/separator/)



