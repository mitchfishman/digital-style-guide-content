Modals are screen-takeover components. 

On desktop, they overlay the screen with a dark color. The modal is then layered on top of the background to provide high contrast and to prevent the user from interacting with other components. If the user clicks on the transparent overlay, the modal should dismiss. If there is a negative action on the modal (such as 'Cancel'), clicking on the transparency should behave as though the user had taken the negative action.
On mobile, modals take over the entire screen. The user may close the modal by pressing the close button in the top right.

There is no limit to the length of a modal.

### Using a modal vs dialog

Modals are for longer content and may include other UI controls, such as accordions or tabs. For shorter confirmation messages or errors, consider using a [dialog](https://styleguide.docs.t-mobile.com/#/components/dialogs/usage) instead.


### Best practices

#### Do

- Use for longer messages with affirmative/negative and dismiss actions.
- Use the `modal-opening button` to launch the modal (do not use a link)

#### Don't

- Use more than one layer of modals (no modals within modals).
- Use when there's another 'overlay style' component on the page.
- Allow interactions with other components on the screen while a modal is active.

### Accessibility

- Test that the focus remains in the modal with the keyboard by tabbing around.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/modal-dialog/)
