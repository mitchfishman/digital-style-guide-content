A progress bar component communicates to the user the progress of a particular process.

### Types of progress bars

#### Default
Display the title of each step. You can configure the maximum number of titles to be displayed before the component automatically changes to the Simple type. 

#### Simple
Only the title of the current step will be displayed. 

### Best practices

#### Do

- Use when there are 3+ steps in a flow. Try to keep your steps in your flow to less than 8. If you have a flow that is greater than 8 steps, you should reconsider the flow to reduce the steps.
- Position progress bars beneath the navigation, and above the title of the page.
- Keep the progress bar persistent while the page content loads.
- Pair the progress bar with some way to navigate forward and backward.
- Use default version and state for all progress bars. You may transition to the "Simple" state only if you meet the requirements. 
- Consider one word per step. While there is no upper character count, it is up to the designer to provide contextual copy that is functional, useful, and visually appealing. However, titles MUST be 8 pixels away from the end of their respective indicator.

#### Don’t

- Make progress bar sticky.
- Use them to indicate system loading, or use them with animation. Progress bars are not loading bars.
- Make progress bars items interactive. Pressing on the different steps should do nothing. 
- Repeat the title of the page in the progress bar title.
- Squash many steps into one page.

### Accessibility

- The progress par itself is not interactive.
- Use `aria-label="Progress bar name"` when there is not a visible title.
- Progress bar should have `role = “progressbar”`.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/progress-bar/)

### Titling conventions
- It should be made a priority to title each step in your flow with no more than ONE word.
- If you need multiple words to title a step, it is recommended you consider the “simple” option which only shows one page title per step. 
- If you must have more than one word per step AND choose not to use the “Simple” progress bar, you are strictly limited to 2 words and an ampersand (&). For example, Shipping & Payment. 

### Spanish

Spanish words are typically longer than English words. So, **you must consider Spanish translation** while designing and testing progress bars.

<!-- #### Behavior

On mobile, the user should be able to use a swipe gesture to move to the previous step in the wizard. The user cannot swipe forward to a step they haven't been on. The user cannot swipe all the way out of the wizard, either. The back gesture should do nothing on the first page of a wizard. -->
