Buttons are designed for users to take action on a page or a screen. They communicate to the user that they have a choice. A button’s level of emphasis helps determine its appearance, typography, and placement.

When to use a button:
- To submit a form
- Begin a new task
- Trigger a new UI element to appear on the page
- Specify a new or next step in a process

### Best practices
#### Do
- Identifiable: Should indicate that they can trigger an action.
- Findable: Easy to find among other elements, including other buttons.
- Clear: A button’s action should be clear. Must clearly communicate what to expect on the next step.
- Use sentence case and no punctuation in button.

#### Don't
- Use more than a few words or one line of text.
- Use overly complicated tech-speak, internal enterprise terms, or industry jargon.
- Give the user a feeling of confusion, hesitation, or doubt.


### Types of buttons

#### Primary

For the most prominent call to action. Limit to one per screen. 

Preferred button sizing for primary/secondary lockups like this is 48px high per button with 16px spacing in between buttons. Buttons should have a clearance of 24px at least to provide a clean touch target. Buttons should be stacked on mobile.

#### Secondary

Secondary buttons contain actions that are important, but aren’t the primary action. 


#### Tertiary

For less prominent actions. Can be used alone or grouped with a secondary button. Types of tertiary include primary, secondary, and back.

#### Action

For even less prominet actions.

#### Modal-opening

Modal-opening buttons are used to open a modal on the same page. This pattern replaces the tooltip design, which T-Mobile do not support anymore.

### Accessibility
- **If it goes somewhere, it's a link.** – `<a href="contact">Contact</a>`
- **If it does something, it's a button.** – `<button>Menu</button>`
- Do not use primary or secondary buttons as navigational elements. In the case of taking the user to another page, please use the tertiary button *style* but with `<a>` tag.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/button/)
