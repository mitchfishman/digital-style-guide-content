### Let’s work together

When planning for a feature related to a user interface, please make sure the Design System review process is included in your schedule. Follow the steps below and we’ll provide design, development, and accessibility feedback before your designs are passed to development. Then, we’ll pair your dev team with a SuperGlue dev to answer questions about accessibility, global styles, and reusable components.

### Why this is important

Features can be pulled from releases last minute if they don’t meet accessibility and design requirements. No one wants this to happen. It can be avoided by solving design problems early on, through the intake and review process.

### The intake process

1.  Please create a dependency ticket in Jira Align, especially during PI Planning. It’s good to have a placeholder dependency even if the design comps are not finalized.

- Delivery group: Technology Ecosystems DG
- Team: CDCDWR2:FLEx_Super GLUE

2. Ask UX Designers to [sign up for design review & a UI dev review](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=svIXJR).

![Jira Align dependency example](https://gitlab.com/mitchfishman/digital-style-guide-content/-/raw/master/assets/images/jira-align-sg-dependency.png)

### The review process

There are two steps to the review process, the UX review, and the the development and accessibility review.

##### UX Design Review

This can be during the weekly UX review meeting or during office hours. [Sign up for a review](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=svIXJR).

##### Development and Accessibility Review

For this meeting, the designer, product owner, and developer on your team should join.
[Sign up for the weekly review meeting](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=svIXJR)

### Resources

More on what is Design Systems are and how our partnership benefits Digital: [View the PowerPoint](https://tmobileusa.sharepoint.com/:p:/s/GELProject/ETEUbRloU51GiBlZrdXNaO0B-pCGOX-_r88AyMhjafr8eA?e=xUMv7F)

You can also reach out to Vivian Huang or Mitch Fishman, or post in #help-design-systems.

![Chart of Super Glue workflow and collaboration model](https://gitlab.com/mitchfishman/digital-style-guide-content/-/raw/master/assets/images/collaboration-model.png)

![Review process flowchart](https://gitlab.com/mitchfishman/digital-style-guide-content/-/raw/master/assets/images/review-process-flowchart.png)
