<div class="banner banner-warning"><!----><div class="banner-icon" style=""><mat-icon class="mat-icon material-icons mat-icon-no-color" role="img" aria-hidden="true">error_outline</mat-icon></div><div class="banner-content"> This <strong>Infographic</strong> section is incomplete and still under construction and consideration.</div></div>

## What are infographics?

Infographics display data to the user through graphical visualizations. Infographics can be interactive. They should be used to make trends and patterns in data easily observable. Infographics should be custom designed for the context they are in, and because of this we do not have templates for them.

## Best practices

Dos

- Label graph axes
- Make graphics visually appealing but also easily understandable

Donts

- Use misleading scales (ex: logarithmic instead of linear)
- Let users interact and play with the data
