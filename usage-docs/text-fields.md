Text fields allow users to input information and interact with the site, whether it is a single text field or a longer sign-up form. Different field types can bring up different virtual keyboards on a mobile device. Text fields are very often used in association with Dropdowns.

Text fields allow the user to input text via the keyboard. The user must first click, tap or tab (using the keyboard) on the text field and then begin to type. The user's input appears in the text field as they type.

A developer can choose the right *type* of input that will bring up the appropriate keyboard on mobile. When designing, we should call out whether the field will be something other than standard text. 
* `number`, which can bring up the numerical keyboard
* `tel`, for phone numbers, can bring up the numerical keyboard with keys that you might use on a phone
* `email`, which can show a keyboard with a "@" and common domains; on some desktop browsers, it will do minimal validation as well
* `search`, which can include a spyglass button to execute the search; on some browsers, it presents some extra UI that we have styled
* `url`, which shows a keyboard with a "/" and common top-level domains like *.com*

### Best practices

#### Do

- Break up long forms whenever possible with contextual titles or sets.
- On mobile, use fewer forms if possible. For example, use "Name" rather than "First name" and "Last name".
- Consider error messaging when designing interfaces that use text fields.

#### Don't

- No don'ts here.

### Accessibility

- Always provide a descriptive label.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/text-input/)

<!-- ### Anatomy

- Text fields are 80px high.
- Desktop recommended width: 2, 3, 6 columns
- Mobile recommended width: 12 columns
- On mobile, form fields should be 12 columns wide. -->

<!-- ### Errors

- Errors from typing invalid text input are considered low priority.
- Errors show a red alert icon underneath the text field.
- For accessibility, error messages present on text fields must contain "Error: " before the error message text.

### Variations

- Text fields can include an icon on the right side. This is often used for payments types.
- Text fields may include helper text or a hyperlink underneath. For example helper text might say "Please type in a valid email address." or "No special characters allowed."
- Disabled form fields use a dotted line instead of a solid line. -->

<!-- For additional information on best practices for form design, check out some additional resources here:

https://www.ventureharbour.com/form-design-best-practices/

https://www.formassembly.com/blog/web-form-design/

https://www.nngroup.com/articles/web-form-design/

https://medium.theuxblog.com/10-best-practices-for-designing-user-friendly-forms-fa0ba7c3e01f

https://www.smashingmagazine.com/2018/08/ux-html5-mobile-form-part-2/ -->
