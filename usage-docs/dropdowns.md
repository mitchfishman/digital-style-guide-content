Dropdowns are a selection control that displays an active selection from a set of possible options. On the web, the selected field opens a menu from which the user may select option(s). For native mobile apps, use an OS-appropriate selection tool – [iOS](https://developer.apple.com/design/human-interface-guidelines/ios/controls/pickers/) /
[Android](https://material.io/components/menus/android).

### Best practices

#### Do

- Use dropdowns when there are more than 3 options available. Otherwise, consider using radio buttons/checkboxes instead.
- Consider if your dropdown could be replaced by a text field (ex: entering birth year).
- Support keyboard input for quick selection.

#### Don’t

- Use dropdowns excessively. Many components can resolve the same problem as a dropdown more elegantly. The advantage of dropdowns is their small footprint.

### Accessibility
- Dropdowns must include the directive `tmoDocsOptionReader` to ensure menu options are read by screen readers. Updates for the screen reader may be complex interactions for developers. Please read the examples and API documentation on the Style Guide carefully and reach out to the SuperGlue team for code reviews. - 
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/select/)

