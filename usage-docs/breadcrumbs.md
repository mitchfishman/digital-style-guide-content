Breadcrumbs are used to track the user's position within the page hierarchy. This is mostly valuable in sub-pages multiple layers deeper than the home page. Breadcrumbs are interactive. If a page title is too long, or the user is using a mobile device, previous pages can be bundled into a dropdown menu.

Breadcrumbs aren't required but best used if they will augment the page's usability.

Clicking or tapping on a breadcrumb must navigate the user to the page the breadcrumb represents.

### Best practices

#### Do

- Use on pages several layers deep within the site map.
- Use the page titles for breadcrumbs.
- Place them between the header and the page title.

#### Don't

- Use Breadcrumbs to track the user's navigation path. They track the user's location within the site map, regardless of which page the user came from previously.
- Use excessively long page titles for breadcrumbs.

### Accessibility

- Pressing the Tab key should move focus to the next interactive link.
- When focusing on the ellipsis menu, pressing Enter or Spacebar should open the ellipsis menu, and arrow keys should allow the user to choose another breadcrumb


### SEO
- The breadcrumbs are marked up with machine-readable [BreadcrumbList Microdata](https://schema.org/BreadcrumbList) such as `item` and `name`
