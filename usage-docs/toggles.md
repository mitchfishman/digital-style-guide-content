Toggles are used to signify whether a given option (represented by an associated label) is on or off.

### Best practices

#### Do
- Stack multiple toggles vertically.
- Use toggles for settings with a higher level of permanence.

#### Don't
- No don'ts here.

### Accessibility
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/toggle-switch/)
