Tables organize data into grids for enhanced readability. Note that tables can vary widely based on data type and so there is currently no one standard visual design. However, the best practices and accessibility notes should still apply.

### Best practices

#### Do

- Clearly label rows and columns.
- Use appropriate units of measure.

#### Don't

- Use tables to display non-data content.

### Accessibility

- Logical focus order and updates for the screen reader may be complex interactions for developers. Please work closely with the SuperGlue team to prototype the accessible experience.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/figure/)
