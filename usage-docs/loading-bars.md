Loading bars provide feedback to the user that a request is currently being processed. Loading bars appear underneath the header. 

### Best practices

#### Do

- Use determinate loading bars when the operation will take a known amount of time to complete.
- Use indeterminate loading bars when the operation will take an unknown amount of time to complete.
- Use loading bars when an operation takes longer than 1/10th of a second to complete. [Reference](https://www.nngroup.com/articles/response-times-3-important-limits/)
- Use Loading bars for page-level navigation load times.

#### Don’t

- Position loading bars anywhere other than beneath the header. If you find yourself needing to position a loading bar elsewhere, consider using a Spinner instead.

### Accessibility

- Loading bars usually are not focusable.
