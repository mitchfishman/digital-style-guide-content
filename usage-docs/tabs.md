Tabs organize and allow navigation between groups of content that are related and at the same level of hierarchy.

### Best practices

#### Do

- Use tabs to group information object that are related.
- Order the tabs based on user needs.

#### Don’t

- Use multiple rows of tabs. Tabs should be presented in one row.
- Create more than 1 navigation level under tabs as their relationship may be unclear to users.
- Use tabs when users need to compare the content behind them.
- Change anything else on the page when navigating to another tab.
- Use more than six tabs. If more than six tabs are needed, consider other navigation patterns.

### Accessibility

- In the tab controls area, the LEFT_ARROW and RIGHT_ARROW keys should switch focus to the adjacent tabs. SPACE and ENTER should activate the tab and display the tab panel.
- A user should be able to use the TAB key to navigate to the tab controls area, then to the tab panel, and then to the focusable items inside the tab panel. See [W3.org](https://www.w3.org/TR/wai-aria-practices/examples/tabs/tabs-2/tabs.html) for an example. To accomplish this with the Angular Material's mat-tab-group component, developers need to nest all of the tab panel content inside of a new element (a div is fine) and add the attribute tabindex="0" to enable keyboard focus.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/figure/)

