## Determinate Spinner

HTML file: progress-spinner.component.html

```
<mat-progress-spinner
  color="primary"
  mode="determinate"
  [strokeWidth]="stroke"
  [diameter]="size"
  [value]="progress">
</mat-progress-spinner>
```     

## Indeterminate Spinner

HTML file: progress-spinner.component.html

```  
<mat-progress-spinner
  color="primary"
  mode="indeterminate"
  [strokeWidth]="stroke"
  [diameter]="size">
</mat-progress-spinner>
```     

## Determinate Bar

HTML file: progress-spinner.component.html

```  
<mat-progress-bar
  color="primary"
  mode="determinate"
  [value]="progress">
</mat-progress-bar>
```          

## Indeterminate Bar

HTML file: progress-spinner.component.html

```  
<mat-progress-bar
  color="primary"
  mode="indeterminate"
  [value]="progress">
</mat-progress-bar>
 ```       
