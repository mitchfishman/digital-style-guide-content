## Why Design Tokens?

The main purpose of design tokens is to enable designers and developers to speak the same language. When discussing a change, both parties can know what parts of the system will be affected.

### Types of Tokens

For the web, design tokens are output as css variables that have 3 different types:

- **Global:** These tokens are base values that all other tokens are based on. Typically developers will not be using these directly in their code. i.e. `--color-magenta-default: #e20074;`
- **Alias:** These tokens reference global tokens and add a layer of meaning. There are different sets of alias tokens for each theme. i.e. `--color-brand: var(--color-magenta-default);`
- **Component:** These tokens are defined at the component level (or in mixins) and are very targeted in their application. They use `alias` type tokens for their values. i.e. `--color-primary-button-background: var(--color-brand);`
