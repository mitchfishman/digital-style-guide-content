### Overview

In order for development and authoring teams to easily format money in a consistent way, we have provided a tool that will programatically take a whole number, such as 123.45 and output the amount based on UX's business rules.

Money formatting pipe formats the given price as per the provided user parameters. The money pipe adds the default currency symbol, i.e., **'$' symbol**. It takes three optional parameters, which must be used in the below sequence - 
1. The first parameter when set to true will always format the amount with a comma, whereas if it is set to false, it will only show a comma for numbers greater than 9999. 
2. The second parameter when set to true will always format the amount to show cents even if it is a whole number, whereas if it is set to false, it will only show cents for decimal values.
3. The third parameter when set to true will add a minus sign (not a hyphen) prefix to the number to indicate a negative number. The minus sign will be read as "minus" while using screen reader.




| Data | Show Comma | Show Cents | Show Minus | Default: no comma <9,999, hide cents if 00 |
| --- | --- | --- |--- |--- |
| 10  | $10  | $10.00 | -$10 | $10 |
| 9.5 | $9.50 | $9.50 | -$9.50 | $9.50 |
| 1234.567 | $1,234.57 | $1234.57 | -$1234.57 |1234.57 |
| 9998 | $9,998 | $9998.00 | -$9998 | $9998 |
| 9999 | $9,999 | $9,999.00 | -$9,999 | $9,999 |
| 50123.22 | $50,123.22 | $50,123.22 | -$50,123.22 | $50,123.22 | 


### Example
Enter amounts in different formats, with and without decimal. You can use the toggles to change the output behavior. Designers can use this knowledge to annotate designs to give the desired look.
<!-- example(money-pipe) -->
