Progress circle is designed to show the current status of a given activity. It is different from the progress bar in which users can move forward and backward in the steps. The progress circle displays information only and does not allow the user to interact with the component directly. 

### Accessibility

- Do not define the role as a progress bar.
- The component name is hidden from the component level so the screen reader will not read out the name. Instead, the screen reader will read out the number value, and the unit, if specified in the scenario. Refer to the examples and API tab for common scenarios.

