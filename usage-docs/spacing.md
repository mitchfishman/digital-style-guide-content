
Our design system is meant to have spacing as part of the elements that are published. We try to use the established spacing before customizing the spacing using these tools. However, some layouts need extra spacing to create an ideal experience. 

### Spacing between elements

- Header and title: 32px/40px (M)
- Title and body copy: 16px (XS)
- Between Primary & Secondary CTAs: 16px (XS)
- Body copy and next element: 40px (L)
- General: 24px
- CTAs and next interactive element: 24px (at least)
- Tertiary buttons: 24px (including label)

#### In lists

- Section headers and list content underneath: 8px
- Section headers and copy underneath: 8px
- Space between sections: 24px
- Space between list item and legal text: 16px

<!-- <img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=lfPzycYyMD&entryId=1ad88133-867e-4a1e-99b0-321b714f68f7&cb=1570479601270" width=300px/> -->

### Further reading

To aid in a better handoff, Designers and Developers can deeply familiarize themselves with line-height, padding, and margins, to understand how best to communicate extra spacing to existing elements.

[*Padding vs. Margin*](https://uxengineer.com/padding-vs-margin/) <small>UX Engineer</small>

[*Demystifying Line Height on the Web - Part 1*](https://medium.com/sketch-app-sources/demystifying-line-height-on-the-web-part-1-c4a0c1328e4d) <small>Sketch app</small>

[*A Harmonious Spacing System for Faster Design-Dev Handoff*](https://blog.marvelapp.com/harmonious-spacing-system-faster-design-dev-handoff/) <small>Marvelapp Blog</small>
