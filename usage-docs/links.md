Links are used to navigate the user to a different location. 

### Accessibility

- The inner text should describe the purpose of the link. Do not repeat the inner text content of a link in the `aria-label`.
- **If it goes somewhere, it's a link.** – `<a href="contact">Contact</a>`
- **If it does something, it's a button.** – `<button>Menu</button>`
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/link/)
