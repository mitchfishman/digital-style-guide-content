### What are Banners?

Banners are used to draw the user's attention to high-importance information.

Banners span the full width of the page. Banners may contain text, icon, and tertiary button. They cannot contain any other controls.

Banners are placed immediately underneath the Header. Banners push content down. They do not overlap.

### Types of Banners

#### Magenta (Preferred)

Use this in most cases. Appropriate messaging topics include:

- Promotional materials
- New features
- Affirmative messaging
- Benefits added to account

#### Grey

Use this for banner content that does not demand the user's attention.

#### Red

Use this for errors:

- Autopay failure.
- Account suspension.
- Bill overdue.
- Other account-level errors that demand the user's attention.
