A dialog is a type of modal window that appears in front of content to provide critical information or ask for a decision. 
Dialogs disable interactions with all web functionality when they appear, and remain on screen until confirmed, or until a required action has been taken.
They are primarily used for lightweight creation or editing tasks, and simple management tasks. Dialogs are purposefully interruptive, so they should be used sparingly.

Dialog is part of the [notification framework](https://styleguide.docs.t-mobile.com/#/frameworks/notification-framework/usage). 


### Using a dialog vs modal

Use dialog when:

- There is a message under 200 characters
- There is an answer needed from the user to progress forward

In other cases, consider using a [modal](https://styleguide.docs.t-mobile.com/#/components/modals/usage).


### Behavior

#### Interaction
Dialogs can appear without warning, requiring users to stop their current task.

#### Scrolling
Most dialog content should avoid scrolling. When scrolling is required, the dialog title is pinned at the top, with buttons pinned at the bottom. This ensures selected content remains visible alongside the title and buttons, even upon scroll.
Dialogs don’t scroll with elements outside of the dialog, such as the background.

#### Position 
Dialogs appear at the center of the screen.
Dialogs retain focus until an action has been taken. 

#### Dismissing dialogs
If the user’s ability to dismiss a dialog is disabled, the user must choose a dialog action to proceed.

### Best practices

#### Do

- If a single action is provided, it must be an acknowledgment action.
- Use Dialogs for quick, actionable interactions, such as making a choice or asking the user to provide information. If two actions are provided, one must be a confirming action, and the other a dismissing action.
- Only include information needed to help users make a decision. If more extensive information is needed, provide it prior to entering the dialog.
- Button text should reflect the actions available to the user (e.g. save, delete).
- Validate that the user’s entries are acceptable before closing the Dialog. Show an inline validation error near the field they must correct.
- Body text should be limited to approximately 200 characters.


#### Don't

- Use more than one layer of dialog (no dialogs within dialogs). Consider taking the user to a new page.
- Use when there's another 'overlay style' component on the page.
- Allow interactions with other components on the screen while a dialog is active.
- Use Dialogs for informational purposes, when the user doesn't need to take any action. Instead, use the Modal component.
- Include the close "X" icon.
- Include other UI controls or images.
- Provide a third action such as “Learn more” as it navigates the user away from the dialog, leaving the dialog task unfinished.


### Accessibility

- Dialogs has `role="dialog"` on the root element by default. The role can be changed to alertdialog via the MatDialogConfig.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/modal-dialog/)

