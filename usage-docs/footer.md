The universal footer appears at the bottom of the page and provides navigation links to the different parts of the site. On mobile, the links collapse into groups.

Footers are authorable and should not be hard-coded to any page. Please consult the UNAV governance team for any changes to the footer. (Slack channel: [#help-unav](https://slack.com/app_redirect?channel=C01T7AXDBM1))


### Best practices

#### Do

- Place spacing between the bottom of the content and the footer.

#### Don't

- Place any other content below the footer.

### Accessibility

- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/footer/)
	
