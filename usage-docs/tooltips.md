### What are Tooltips?

Use tooltips to provide definitions or additional information about text-based content in page.

Tooltips display on hover or on press. On mobile, because there is no hover concept, tooltips display on press.

Tooltips will be displayed below the element but this can be configured.

When focused by a screen reader or otherwise, Tooltips should show their content.

### Best practices

- Tooltips should not contain links or controls
- Tooltips should only contain text
- Do not place a tooltip within a tooltip
- Limit tool tips in length
- Use a maximum of one tooltip per paragraph.
- Be mindful that minimum touch target size should be 48x48px.

### Accessibility

- Only interactive elements should trigger tooltips
- Do not put essential information in tooltips
- Do not use a timeout to hide the tooltip
