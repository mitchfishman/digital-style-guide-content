# Overview

* Shares similar functionality and styling as buttons
* Can show a series of page numbers for extensive content. The number of pages if flexible depending on the needs of your project.
* Can be minimized to just "back" and "forward" arrows with NO page numbers if needed
* An alternate version of pagination is available for data-heavy instances

# Best practices
* Do not overcomplicate a design with too many pages displayed at a time. 3 - 5 pages are recommended
* It is recommended to reduce the number of pages displayed on mobile to create a less cluttered look
