Checkboxes signify whether a given option (represented by an associated label) is selected or not.

Checkboxes are most effectively used for opt-in options (like on terms & conditions) or lists where the user can select any number of options from a set.


### Best practices

#### Do

- Stack options vertically (preferred).
- Place control left of the label.

#### Don't

- No don'ts here.

### Accessibility

- Checkboxes without text or labels should be given a meaningful label via aria-label or aria-labelledby.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/checkbox/)

