Pagination appears below the content of a page and indicates to the user that there are more pages of content beyond this one. 

### Best practices

#### Do

- Use to break up lists, such as e-commerce category pages, search engine results pages, article archives, and image galleries.

#### Don't

- Use to break up articles. Use a single page for all article content.


### Accessibility

- Refer to the example on the Style Guide for different states, including the enhanced visual indicator for focus states.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/pagination/)
