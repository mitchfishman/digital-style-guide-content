Notification banners are used to draw the user's attention to high-importance information.

### Best practices

#### Do
- Banners can only may contain text, icon, close button, and tertiary buttons/links.
- Banners span the full width of the page. 
- Banners are placed immediately underneath the header. If there is a progress bar, they may overlap the progress bar. Other page content should be pushed down. 
- Banners can be optionally dismissed and will animate away, either by a close button being pressed, or by setting a timer for auto-dismissal.

#### Don’t
- Don't place banners anywhere else than underneath the header. 

### Types of notifications

#### Brand color

This is the main brand color (magenta for T-Mobile/orchid for Metro). Appropriate messaging topics include:

- Promotional materials
- New features
- Affirmative messaging
- Benefits added to account

#### Subtle

Use this muted-color banner for content that does not demand the user's attention.

#### Error

A red banner to use for medium-priority errors.

- Autopay failure.
- Account suspension.
- Bill overdue.
- Other account-level errors that demand the user's attention.


#### Warning

A yellow banner to use for low-priority errors or temporary issues.

#### Dark

A style alternative to Subtle. This will be dark in both light and dark themes.

### Accessibility
- Focus does not automatically move to the banner, but can move to interactive elements within the banner.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/alert/)


