## Best practices

T-Mobile follows material guidelines for icon styling. We keep it consistent by prioritizing a list of pre-defined icons. If you can't find an icon you're looking for in this style guide, please create an intake in Slack `#help-design-systems` channel.

Our preferred icon size for actionable buttons is 24 x 24px. For consistency, try to make your icons this size. In the case where the icon is not actionable, it can be any size. Our icon ramp follows:

* 16 x 16 px
* 24 x 24 px

In almost all cases, actionable icons should be used with a label. There are a few exceptions to this rule:

* Menus
* Icons within a floating action button

Actionable icons can be black, grey, or magenta. Warning icons may be used in red. On dark backgrounds, icons may be white. Icons cannot be used in any other color.
	
Future content: when to use illustration vs icon.

## Available Icons

The preferred way is to use the SVG icons exported as part of the `@tmo/components` package. Using the Google Material font icons is still an option.
All available icons can be found on the [examples tab of this page](/#/foundation/icons/examples).
