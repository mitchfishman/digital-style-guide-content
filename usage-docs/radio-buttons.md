Radio buttons are a selection control that allows the user to select one option from a number of other mutually exclusive options. 

### Best practices

#### Do

- Make sure the label is also be interactive, which means the label area is focusable, clickable, and tappable.
- Stack radio buttons vertically.

#### Don’t
- Add excessive interactive elements in-between radio buttons.

### Variations of radio buttons

#### Radio group
In the case that a label for a radio button has a paragraph description, the selection control should be aligned with the label. 

#### Styled radio buttons
Alternatively, radio selections can be styled without the typical circle control. Users will be able to select from one of the options that are styled like buttons or chips.

#### Radio buttons with expansion panel
Normally, you can't traverse radio inputs with the tab key, and once a radio is selected, the other radios aren’t focusable. This custom component lets users tab through radios and their associated dropdown buttons from left to right. The additional buttons on the right allow users to activate other controls like expanding the content or editing input.

#### Card radio group
The custom component is optimized for the scenario when radio options are styled as cards. There are multiple examples of different label positions. Refer to the [Card Radio Group](https://styleguide.docs.t-mobile.com/#/components/card-radio-group/examples) component.

### Accessibility
- The pattern to have radio buttons with interactive elements in-between is generally not encouraged as this would disrupt the typical radio group keyboard experiences. Please work closely with the Accessibility Team for special cases.
- Using semantic HTML with <fieldset><legend> provides the most accessibility features by default.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/radio/)

