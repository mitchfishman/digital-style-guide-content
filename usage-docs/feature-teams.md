In Digital, a **Feature team** is typically a scrum team working in a specific Value Stream. T-Mobile's Design Systems process usually begins with the Product Manager/Owner submit a dependency ticket during quarterly PI planning, followed by a Journey's UX Designer reviewing designs with Design Systems' Designers. Once near final, the next step is to sign-up UI dev and accessibility review with the Design System governance team and Super Glue team to jump in and kickstart feature development.

Ready for code to start hitting the screen? Please use our [sign-up sheet](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=ZXP4uS) or contact us on our [#help-design-systems](https://slack.com/app_redirect?channel=CNLCRK48K) slack channel to get started!

### About the Super Glue team 
The Super Glue team is a scrum team under the Ecosystem Foundations Value Stream. It is made up of developers who, along with the product and design team, manage T-Mobile's Design System. 

**Our goal is to empower feature-team developers.** We accomplish this by: 

* Developing [global stylesheets](https://gitlab.com/tmobile/digital/ux/style-guide/-/tree/tmo/master/libs/shared/ui/tmo-styles) *(GitLab)*
* Creating [reusable components ](https://gitlab.com/tmobile/digital/ux/style-guide/-/tree/tmo/master/libs/shared/ui/components) *(GitLab)*
* Developing accessible user interfaces 
* Helping feature-team developers create their own user interfaces, using these global stylesheets and reusable components. (more on this below)

### Pair programming with Super Glue developers 
Once a user interface has been reviewed and approved by the Design System team, the feature team devs will be paired up with a Super Glue dev, who will answer questions, provide examples, and pair program as needed. They’ll provide guidance on:

* Laying out the page as a [grid](/#/foundation/grids/examples) using Blueprint CSS
* Which reusable components to use and how to configure them 
* Which helper classes and scss variables to use for padding, margin, color, etc.
* Best practices


### Does the Super Glue team still deliver web page UIs? 
No, our team is here to help you while you put together your project UIs! If needed, we can create prototypes in the playground application of the style guide repo to demo important functionalities. 

Follow this process to access the code and test it in your local environment: 

```
Clone the style-guide repo: https://gitlab.com/tmobile/digital/ux/style-guide 
cd style-guide 
git checkout tmo/master 
git fetch 
npm install 
npm run start:playground 
Then visit http://localhost:3070/prototypes/[routename] 
```
You can find the route name in the playground's [app.module.ts file](https://gitlab.com/tmobile/digital/ux/style-guide/blob/f8efb2394345399302ef839ba0e6649dc81f3557/apps/playground/src/app/app.module.ts). *(GitLab)*

#### The Super Glue dev will:  

* Provide the file paths of the directories they need to copy into tos-apps. 
* Provide the localhost URLs so the devs can learn more about the UIs.
* Review merge request containing UI components and provide feedback.
* Answer questions and be helpful.

### Reusable components 
These are in the [components section of the style-guide repo](https://gitlab.com/tmobile/digital/ux/style-guide/-/tree/tmo/master/libs/shared/ui/components). Each component also has a page on the style guide website. 

If you want to dig into the components’ code in tos-apps, you’ll find the code is obfuscated and not understandable. This is because of how the components are packaged.

If you want to explore the code, you need to do that in the style-guide repo. For this reason, we recommend feature devs clone the style-guide repo and become familiar with it. 

### Global styles 
T-Mobile's global styles are in the [tmo-styles section of the style-guide repo](https://gitlab.com/tmobile/digital/ux/style-guide/-/tree/tmo/master/libs/shared/ui/tmo-styles). You can also find them in tos-apps under node_modules/@tmo.

### Classes, SCSS Variables, and Mixins 

It’s a good use of time to become familiar with [tmo-styles](https://gitlab.com/tmobile/digital/ux/style-guide/-/tree/tmo/master/libs/shared/ui/tmo-styles) and look through each file. The same applies to styleguide.t-mobile.com. Nonetheless, here’s a cheat sheet: 

* For colors, use the variables defined in [_colors.scss](https://gitlab.com/tmobile/digital/ux/style-guide/blob/5fb9f0429415c3ffef4ccd1537288c7ae50bf4d2/libs/shared/ui/tmo-styles/src/lib/config/_colors.scss#L1), such as $tmo-magenta-brand. Colors should only be hardcoded if instructed by the designer to use rgba.
 
* To apply padding and margin in an html file, use the helper classes referenced in our [Spacing guide](/#/foundation/spacing/api). 

* To apply padding and margins in a scss file, use spacing variables like $spacing-xs whenever possible and use REMs when there isn’t a variable available. You can also use the [rem() utility function](https://gitlab.com/tmobile/digital/ux/style-guide/blob/366537edbf10da96d54ace2defd131711f523779/libs/shared/ui/tmo-styles/src/lib/config/_sass-utilities.scss#L5). **Avoid using pixels (px) for padding and margins**. 
 
* For font sizes and styles, see the [Typography API page](http://localhost:4200/foundation/typography/api). 
 
* Instead of using media queries, @include the mixins for-phone-only, for-tablet-up, etc. found in [_breakpoints.scss](https://gitlab.com/tmobile/digital/ux/style-guide/blob/3ac66d6d658b84f8343fc224af8db79e31a0e153/libs/shared/ui/tmo-styles/src/lib/config/_breakpoints.scss). 
 
* We use our own [icon sets](/#/foundation/icons) and use `<tmo-icon>iconName="icon-name"</tmo-icon>`  in most cases. 
	
* If you’re putting the icon in the ::before or ::after pseudo-elements, you will need to @include in the [material-icons or material-icons-outlined mixins](https://gitlab.com/tmobile/digital/ux/style-guide/blob/366537edbf10da96d54ace2defd131711f523779/libs/shared/ui/tmo-styles/src/lib/config/_sass-utilities.scss#L9), as seen in the [.open-modal-link class](https://gitlab.com/tmobile/digital/ux/style-guide/blob/e840cbc059867a7005a469d57cc519a62aba98c4/libs/shared/ui/tmo-styles/src/lib/themes/tmo-shared/_buttons.scss#L268).
 
* *.hidden* hides elements visually but still makes them available to screen readers. This replaces the *.sr-only* class. Check out the progress bar for an example. 
 
*  *.full-width* sets an element to `width: 100%` 

### UI Kit 
The detailed design properties are documented in the [UI Kit](https://www.figma.com/file/o5PpYaruvoRiqWdH9eZtia/Web-UI-Components?node-id=0%3A1). The kit is an evolving document. It’s often a good idea to communicate directly with the designer and Super Glue dev before, during, and after you code. 
	
### Working with the Accessibility (A11y) team 
The Design System teams work very closely with the accessibility team as they establish global styles, reusable components, and review web page layouts. We need their constant feedback to make sure we’re meeting A11y standards, both for legal protection and to provide the best user experience. 

As feature teams create their own web page layouts, they should also be following A11y standards. 

Questions are often asked in the [#help-accessibility-digital](https://slack.com/app_redirect?channel=CQ6PW2KN1) slack channel but you can reach out to Bobby Roe and Charlie Triplett directly as well.  

Here are T-Mobile's [a11y development guidelines](https://www.magentaa11y.com/) for web and app.  

For testing, please use JAWS or NVDA for Windows, and VoiceOver with Safari for Mac. These tools are used to verify the accessibility of your UI. Please also take the [Google course on Accessibility](https://www.udacity.com/course/web-accessibility--ud891) to get up to speed. 
	

### Communicating with the Super Glue and Design System teams 
A great place to start is the [#help-design-systems](https://slack.com/app_redirect?channel=CNLCRK48K) slack channel. Don't be afraid to ask questions. We are here to help you!
Sign up for a review in our [Sign-up Sheet](https://tmobileusa.sharepoint.com/:x:/s/GELProject/EZF_SuvnT-ZJvMDqA7q8780BvAj6UIYNh1rpVWipCU2WRg?e=ZXP4uS)

The Product Owner of the Super Glue team is Vivian Huang and our Product Manager is Mitch Fishman. They are also glad to help!
