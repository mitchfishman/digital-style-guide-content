Datepickers allow a user to select a single calendar date. Datepickers appear as a dropdown below a form field. Users may select the date from the dropdown or use the keyboard to type it in manually.

### Best practices

#### Do

- Use the date picker when the user must select a date.
- If the user must select multiple dates, use multiple date pickers.


#### Don’t

- Use the date picker without a dropdown box.


### Accessibility

- Refer to the example on the Style Guide for different states, including the enhanced visual indicator for focus states.
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/date-picker/)

