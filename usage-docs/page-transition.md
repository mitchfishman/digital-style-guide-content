Animations enhance user experience by providing user the hint of change on the page.

### Best practices

#### Do

- Use animations for page transition.
- Use slide animations for content that appears after the page load.

#### Don’t
- No don'ts.

### Accessibility
- More info at [A11yEngineer.com](https://www.magentaa11y.com/checklist-web/animation/)
