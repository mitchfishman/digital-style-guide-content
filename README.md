# Digital Style Guide - Documentation Content Git Repository

The project is a git repo to manage the authored content of [Digital Style Guide](https://gitlab.com/digital-style-guide/digital-style-guide.git)

The original structure of `Digital Style Guide` is

```sh
<workspace name>/
├── apps
│   ├── style-guide
│   │   ├── client
│   │   ├── client-e2e
│   │   ├── server
├── libs
│   ├── shared
│   │   ├── components
│   │   ├── examples
│   │   ├── markdown-editor
│   ├── style-guide
│   │   ├── nest-core
│   │   ├── pages
│   │   ├── pipes
│   │   │-─ routes
│   │   │-─ services
│   │   │-─ shared
│   │   │-─ testing
├── docs-content
│   ├── api-docs --> Moves to
│   ├── examples
│   ├── usage-docs
```

This project hold the content of
```sh
├── docs-content
│   ├── api-docs
│   ├── usage-docs
```
